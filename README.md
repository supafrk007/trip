# Trip Project #

### What is this repository for? ###

This is a personal project to create a small site that makes it easy for friends to plan a trip together. It uses facebook authentication and foursquare venues api for locations. To figure out the geo information it uses data from geonames.org.

This project allowed me to learn python, django, django rest framework, oauth and angular. This references the "cor" python repository which I route for common libraries.

# Current Setup/Features for API #

1. Routes in trip_web/trip_web/urls.py
2. Controllers are referred to as "views" in django (yeah it is confusing) and stored in trip_web/trip/views
	- Venues: Returns a paged list of venues from foursquare.
	
	- Bookmarks: Users can bookmark popular venues and see the bookmarks that their friends have made
	
	- Users: Get/Register a user
	
	- City: Search or get a list of cities from geonames.org
	
	- Trip: A User can plan a trip to a city or add friends and cities to a trip.
	
3. Services for the views (controllers) are stored in trip_web/trip/services. 

	- Venues service is the most interesting and references the foursquare rest api using a service I wrote in the cor library.
	- Cities service references cor library to pull data from geonames.org.

4. Models defined in models.py.

5. Unit tests are in tests folder.

# Angular Features #

1. SPA with index.html in trip_web/templates

2. AngularJS code stored in trip_web/static/

3. Uses bower and node_modules but was before I was using grunt.

4. js/cor stores common functionality

5. App specific code in js/trip

6. Remaining code uses require.js and angular to create UI.
