define([], function () {
	'use strict';

    var base_url = "http://localhost:8000/api";

	return {
	    "bookmark" : base_url + "/venue/:venue_id/bookmark",
	    "city" : base_url + "/city/:city_q",
	    "city_bookmarks" : base_url + "/city/:city_id/bookmarks",
	    "cities" : base_url + "/cities/?q=:q",
	    "trip" : base_url + "/trip/:trip_id",
	    "trip_create" : base_url + "/trip",
	    "users" : base_url + "/users/",
	    "users_get" : base_url + "/users/?fb_token=:fb_token",
	    "venues" : base_url + "/venues"
	};
});