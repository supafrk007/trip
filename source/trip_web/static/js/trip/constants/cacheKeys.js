define([], function () {
	'use strict';

	return {
	    'GCITY' : 'gcity',
	    'GCITY_NAME' : 'gcity_name',
	    'GCITY_ID' : 'gcity_id'

        /*
        this.TRIP = "trip";
        this.TRIP_ID = "trip_id";
        this.FRIEND_DATA = "friend_data";*/
	};
});