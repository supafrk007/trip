define([], function () {
	'use strict';

	return {
	    'city' : '/city/:gCityId',
	    'home' : '/home',
	    'trip' : '/trip/:tripId',
	    'trips' : '/trips'
	};
});