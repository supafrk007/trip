define([], function () {
	'use strict';

	return {
	    'home' : '/static/partials/trip/home.html',
	    'city' : '/static/partials/trip/city.html',
	    'trip' : '/static/partials/trip/trip.html',
	    'venues' : '/static/partials/trip/venues.html'
	};
});