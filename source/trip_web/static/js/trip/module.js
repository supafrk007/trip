define([
	'angular',
	'angularRoute',
	'angularCookies',
	'cor/auth/module',
	'cor/util/module',
	'cor/bootbox/module',
	'trip/constants',
	'trip/controllers',
	'trip/directives',
	'trip/factories',
	'trip/filters',
	'trip/services',
	'trip/constants/partialConstants',
	'trip/constants/routeConstants'
], function (
	ng,
	ngRoute,
	ngCookies,
	authModule,
	utilModule,
	bbModule,
	constants,
	controllers,
	directives,
	factories,
	filters,
	services,
	partials,
	routes
) {
	'use strict';
	
	var tripModule = ng.module('trip', ['ng', 'ngRoute', 'ngCookies', 'cor.auth', 'cor.util', 'cor.bootbox']);
	
	tripModule.config(['$routeProvider', '$locationProvider', '$bootboxProvider', '$httpProvider',
		function ($routeProvider, $locationProvider, $bootboxProvider, $httpProvider) {
			$httpProvider.interceptors.push('trip.services.tokenAuthInterceptor');

			$routeProvider
			    .when(routes.home, {
			      controller: 'trip.controllers.home',
			      controllerAs: 'vm',
			      templateUrl: partials.home
			    })
                .when(routes.city, {
                    templateUrl: partials.city,
                    controller: 'trip.controllers.city' })
                .when(routes.trip, {
                    templateUrl: partials.trip,
                    controller: 'trip.controllers.trip' })
        		.otherwise({ redirectTo : routes.home });

			$locationProvider.html5Mode(true);

			$bootboxProvider.setDefaults({ locale: "en" });
      	}
  	]);

  	tripModule.run(['trip.services.auth',
  	    function(Auth) {
  	        Auth.tryRelogin();
  	    }
    ]);

	tripModule.constant(constants);
	tripModule.controller(controllers);
	tripModule.directive(directives);
	tripModule.factory(factories);
	tripModule.filter(filters);
	tripModule.service(services);
	
	return tripModule;
});
