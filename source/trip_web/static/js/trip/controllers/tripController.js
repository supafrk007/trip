define([], function () {
	'use strict';

	var tripCtrl = function(
	    $scope, $http, $q, $location,
	    $routeParams, $timeout, bootbox, Auth, Trip, Events){

	    $scope.trip = {};
	    $scope.cities = [];
	    $scope.members = [];
	    $scope.curPill = {
	        val: "overview",
	        selectedMenu: ""
	    };
	    $scope.bookmarks = false;
	    $scope.filterCity = null;

	    var rotate = 0;

	    /*
	     * Public Methods
	     */
		$scope.curPillClass = function (val) {
			if ($scope.curPill.val == val) return [ "active" ];
			else return [];
		};

		$scope.curVenueMenuStyle = function (val) {
			if (($scope.curPill.val == "places" || $scope.curPill.val == "bookmarks") &&
				(($scope.curPill.val + "_" + $scope.curPill.selectedMenu) == val))
				return { "font-weight" : "bold" };
			else return {};
		};

		$scope.onPlaces = function (section) {
		    $scope.bookmarks = false;
		    $scope.$broadcast(
		        Events.show_venues, $scope.bookmarks, section, $scope.filterCity.city_id, $scope.trip.id);
            $scope.curPill.val = "places";
            $scope.curPill.selectedMenu = section;
		};

        $scope.onBookmarks = function (section) {
		    $scope.bookmarks = true;
		    $scope.$broadcast(
		        Events.show_venues, $scope.bookmarks, section, $scope.filterCity.city_id, $scope.trip.id);
            $scope.curPill.val = "bookmarks";
            $scope.curPill.selectedMenu = section;
		};

        /*
	     * Private Methods
	     */

        function loadTrip(tripId) {
            Trip.get(tripId).then(
                function(data) {
                    $scope.trip = data.trip;
                    $scope.cities = data.cities;
                    $scope.members = data.members;

                    $scope.filterCity = $scope.cities[0];
                },
                function(errorMsg) {
                    console.log("Oops! Failed!")
                }
            );
        }

		function init() {
		    Auth.isLoggedIn.then(
  	            function() {
  	                var tripId = $routeParams.tripId;
                    loadTrip(tripId);
  	            },
  	            function() {
  	                console.log('Oops! Failed');
  	            }
  	        );
		};

		init();
	};

	tripCtrl.$inject = ['$scope', '$http', '$q',
	                    '$location', '$routeParams', '$timeout', 'bootbox',
	                    'trip.services.auth',
	                    'trip.services.trip',
	                    'trip.constants.events'];
	return tripCtrl;
});