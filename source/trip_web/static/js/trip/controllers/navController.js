define([], function () {
	'use strict';
	
	var navCtrl = function($scope, $http, $location, Auth, City, User, routes, urls){
		var self = this;
	
		$scope.CurUser = User;
		$scope.nav = {
			q_city : ""
		};

        /*
         * Properties
         */
		$scope.IsLoggedIn = function() {
			return User.getToken() != "";
		};

        /*
         * Public Methods
         */
		$scope.login = function() {
		    Auth.login();
		};

		$scope.logout = function() {
		    Auth.logout();
		    $location.path(routes.home);
		    $scope.nav.q_city = "";
		};

		$scope.register = function() {
		    Auth.register();
		};

		$scope.searchCity = function() {
			var p = City.search($scope.nav.q_city);
			p.done(function(gcity) {
				if (gcity == null) {
				    console.log("Couldn't find any city to match query");
					//cor.messages.add("Oops, we couldn't find any city to match your query!");
				}
				else {
				    console.log("Found City");
                    var route = routes.city.replace(':gCityId', gcity.geonameId);
					$location.path(route);
					$scope.nav.q_city = "";
				}
			});
		};
	};
	
	navCtrl.$inject = ['$scope', 
						'$http',
						'$location',
						'trip.services.auth',
						'trip.services.city',
						'trip.services.user',
						'trip.constants.routes',
						'trip.constants.urls'];
	return navCtrl;
});