define([], function () {
	'use strict';

	var cityCtrl = function(
	    $scope, $http, $q, $location,
	    $routeParams, $timeout, bootbox, City, Trip, Events,
	    routes){

	    $scope.gcity = {};
	    $scope.near = "";
	    $scope.curPill = {
	        val: "overview",
	        selectedMenu: ""
	    };
	    $scope.bookmarks = false;

	    /*
	     * Public Methods
	     */
		$scope.curPillClass = function (val) {
			if ($scope.curPill.val == val) return [ "active" ];
			else return [];
		};

		$scope.curVenueMenuStyle = function (val) {
			if (($scope.curPill.val == "places" || $scope.curPill.val == "bookmarks") &&
				(($scope.curPill.val + "_" + $scope.curPill.selectedMenu) == val))
				return { "font-weight" : "bold" };
			else return {};
		};

		$scope.onPlaces = function (section) {
		    $scope.bookmarks = false;
		    $scope.$broadcast(Events.show_venues, $scope.bookmarks, section, $scope.gcity.geonameId, 0);
            $scope.curPill.val = "places";
            $scope.curPill.selectedMenu = section;
		};

        $scope.onBookmarks = function (section) {
		    $scope.bookmarks = true;
		    $scope.$broadcast(Events.show_venues, $scope.bookmarks, section, $scope.gcity.geonameId, 0);
            $scope.curPill.val = "bookmarks";
            $scope.curPill.selectedMenu = section;
		};

		$scope.onStartTrip = function (retry) {
			//Trip.Lib.ForceAuthCheck();

            getTripName(retry).then(
                function(name) {
                    if (name != null) {
						createTrip(name);
					}
                },
                function() {
                    $scope.onStartTrip(true);
                }
            );
		};


        /*
	     * Private Methods
	     */

        function createTrip(name) {
            var p = Trip.create(name, $scope.gcity.geonameId);
            p.then(
                function(data) {
                    var route = routes.trip.replace(':tripId', data.id);
                    $location.path(route);
                },
                function(errorMsg) {
                    console.error(errorMsg);
                }
            );
		}

		function getNearParam(gcity) {
			var near;
			if (gcity.fcode == "PPLX") {
				near = gcity.name + ", " + gcity.countryCode;
			}
			else {
				near = gcity.name + ", " + gcity.adminCode1 + ", " + gcity.countryCode;
			}
			return near;
		}

        function getTripName(retry) {
			var p = "What should we name your trip?";
			var isValid = false;
			var dfd = $q.defer();

			if (retry == true) {
				p = "Invalid Entry: " + p;
			}

			bootbox.prompt(p, function(result) {
				if (result == null) {
					dfd.resolve(null);
				}
				else {
					result = result.trim();
					if (result.length > 0) {
						dfd.resolve(result);
					}
					else {
						dfd.reject();
					}
				}
			});

			return dfd.promise;
		}

		function init() {
		    var p = City.get($routeParams.gCityId);
		    p.done(function(gcity) {
		        console.log(gcity);
		        $scope.gcity = gcity;
		    });
		};

		init();
	};

	cityCtrl.$inject = ['$scope', '$http', '$q',
	                    '$location', '$routeParams', '$timeout', 'bootbox',
	                    'trip.services.city',
	                    'trip.services.trip',
	                    'trip.constants.events',
	                    'trip.constants.routes'];
	return cityCtrl;
});