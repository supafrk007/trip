define(['trip/constants/partialConstants'], function (partials) {
	'use strict';

	var venuesCtrl = function($scope, Events, City, Venues, Pager) {

		var self = this;
		var pageSize = 10;
		$scope.bookmarkDic = {};
		$scope.pageList = null;
		$scope.pageNum = 1;
		$scope.showPages = 10;
		$scope.totalPages = 1;
		$scope.section = "";
		$scope.cityId = 0;
		$scope.tripId = 0;
		$scope.near = "";
		$scope.venues = null;

        /*
         * Events
         */

		$scope.$on(Events.show_venues, function(event, bookmarks, section, cityId, tripId) {
		    $scope.bookmarks = bookmarks;
		    $scope.section = section;
		    $scope.cityId = cityId;
		    $scope.tripId = tripId;

		    if ($scope.bookmarks == false) {
		        var p = City.getNearParam($scope.cityId);
		        p.done(function(near) {
		            $scope.near = near;
		            showVenues(1);
		        });
		    }
		    else {
		        showVenues(1);
		    }
		});

        $scope.clickPage = function(p) {
			showVenues(p);
		};

        $scope.isBookmark = function(v) {
			var c = "fa-star-o";
			if ($scope.bookmarkDic[v['id']] == true) {
				c = "fa-star";
			}
			return c;
		};

		$scope.saveBookmark = function(v) {
			//Trip.Lib.ForceAuthCheck();
            var isActive = true;
			if ($scope.bookmarkDic[v.id] == true) {
				isActive = false;
			}
			saveBookmark(v.id, $scope.cityId, isActive);
		};

        /*
         * Private Methods
         */

		function showVenues(p) {
		    var promise;
		    console.log($scope.bookmarks);
		    if ($scope.tripId == 0 && $scope.bookmarks != true) {
		        promise = Venues.getCityVenues(
		            $scope.cityId,
		            $scope.near,
		            $scope.section,
		            p,
		            pageSize
		        );
		    }
		    else if ($scope.tripId == 0 && $scope.bookmarks == true) {
                promise = Venues.getCityBookmarks(
		            $scope.cityId,
		            $scope.section,
		            p,
		            pageSize
		        );
		    }
		    else if ($scope.tripId > 0 && $scope.bookmarks != true) {
                promise = Venues.getCityVenues(
		            $scope.cityId,
		            $scope.near,
		            $scope.section,
		            p,
		            pageSize
		        );
		    }

            promise.then(
                function(data) {
                    var totalPages = Math.floor((data.total_results / pageSize)+1);
                    $scope.totalPages = totalPages;
                    $scope.venues = data.venues;
                    loadBookmarkDic(data.bookmark_ids);
                    $scope.pageNum = p;
                    $scope.pageList = Pager.getPages(p, totalPages, pageSize);
                    //$scope.$emit(Evt.CITY_PLACE_MENU_UPDATE, $scope.isBookmarks, $scope.section);
                },
                function(errorMsg) {
                    console.error(errorMsg);
                }
            );
		}

		function loadBookmarkDic(b_ids) {
			$scope.bookmarkDic = {};
			$.each($scope.venues, function(index, v) {
				var vid = v['id'];
				var b = false;
				if ($.inArray(vid, b_ids) > -1){
					b = true;
				}
				$scope.bookmarkDic[vid] = b;
			});
		}

		function saveBookmark(venueId, cityId, isActive) {
		    Venues.saveBookmark(venueId, cityId, isActive)
		        .success(function (data, status) {
		            if (status != 200) {
						console.error('Error Saving Bookmark');
						return;
					}
					$scope.bookmarkDic[venueId] = isActive;
		        })
		        .error(function() {
		            console.error("Oops, failed");
		        });
        }
	};

	venuesCtrl.$inject = [
	    '$scope',
	    'trip.constants.events',
	    'trip.services.city',
	    'trip.services.venues',
	    'cor.util.services.pager'
	    ];

	var venuesDirective = function() {
	    return {
            restrict: 'E',
            scope: {
              bookmarks: '@bookmarks'
            },
            controller: venuesCtrl,
            templateUrl: partials.venues
          };
	};

	return venuesDirective;
});