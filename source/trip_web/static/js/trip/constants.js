define([
    'trip/constants/cacheKeys',
    'trip/constants/cookieKeys',
    'trip/constants/eventConstants',
    'trip/constants/partialConstants',
	'trip/constants/routeConstants',
	'trip/constants/urlConstants'
], function (
    cacheKeys,
    cookieKeys,
    eventConstants,
    partialConstants,
	routeConstants,
	urlConstants
) {
	'use strict';

	return {
	    'trip.constants.cacheKeys' : cacheKeys,
	    'trip.constants.cookieKeys' : cookieKeys,
	    'trip.constants.events' : eventConstants,
	    'trip.constants.partials' : partialConstants,
		'trip.constants.routes' : routeConstants,
		'trip.constants.urls' : urlConstants
	};
});