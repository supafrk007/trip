define([], function () {
	'use strict';
	
	var base_url = "http://localhost:8000/api";
	
	return {
		users : base_url + "/users/",
		users_get : base_url + "/users/?fb_token=:fb_token"
	};
});