define([], function () {
	'use strict';

	var venuesService = function($http, $q, urls) {
	    var self = this;

        /*
         * Public Methods
         */

	    self.getCityBookmarks = function(cityId, section, p, ps) {
            var url = urls.city_bookmarks.replace(':city_id', cityId);
            var params = {
                city_id: cityId,
                s: section,
                p: p,
                ps: ps
            };

            return getVenuesPromise(url, params);
        };

        self.getCityVenues = function(cityId, near, section, p, ps) {
            var url = urls.venues;
            var params = {
                city_id: cityId,
                near: near,
                s: section,
                p: p,
                ps: ps
            };

            return getVenuesPromise(url, params);
        };

        self.saveBookmark = function(venueId, cityId, isActive) {
            var url = urls.bookmark.replace(':venue_id', venueId);
            var params = {
                is_active: isActive,
				city_id: cityId
            };
            return $http.put(url, params);
        };

        /*
         * Private Methods
         */

        function getVenuesPromise(url, params) {
            var deferred = $q.defer();
            $http.get(url, { params: params })
                .success(function(data, status) {
                    if (status != 200) {
                        deferred.reject("Oops! Failed");
                    }

                    deferred.resolve(data);
                })
                .error(function(data) {
                    deferred.reject("Oops! Failed");
                });

            return deferred.promise;
        };
	};

	venuesService.$inject = ['$http',
	                        '$q',
	                        'trip.constants.urls'];
	return venuesService;
});