define([], function () {
	'use strict';

	var authInterceptor = function ($injector, $location, $q, routes, User) {
		var self = {
			request: request,
			responseError: responseError
		};

	    return self;

	    function request(config) {
	    	var token = User.getToken();

			if (token != null && token.length > 0) {
				config.headers['Authorization'] = 'Token ' + token;
			}

			return config;
		}

	    function responseError(response) {
	    	if (response.status === 403) {
	    		if (response.data.detail === 'Signature has expired.') {
	    			$location.path(routes.home);
    			}
			}
			return $q.reject(response);
		}
	};

	authInterceptor.$inject = ['$injector', '$location', '$q',
	                           'trip.constants.routes', 'trip.services.user'];
	return authInterceptor;
});