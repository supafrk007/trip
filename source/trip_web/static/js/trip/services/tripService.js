define([], function () {
	'use strict';

	var tripService = function($http, $q, urls) {
	    var self = this;

        /*
         * Public Methods
         */

        self.create = function(name, cityId) {
            var url = urls.trip_create;
            var params = {
                name: name,
                city_id: cityId
            };
            var deferred = $q.defer();

            $http.post(url, params)
                .success(function(data, status) {
                    if (status != 200) {
                        deferred.reject("Oops! Failed");
                    }
                    deferred.resolve(data);
                })
                .error(function(data) {
                    deferred.reject("Oops! Failed");
                });

            return deferred.promise;
        };

        self.get = function(tripId) {
            var url = urls.trip.replace(':trip_id', tripId);
            var deferred = $q.defer();

            $http.get(url)
                .success(function(data, status) {
                    if (status != 200) {
                        deferred.reject("Oops! Failed");
                    }
                    deferred.resolve(data);
                })
                .error(function(data) {
                    deferred.reject("Oops! Failed");
                });

            return deferred.promise;
        };
	};

	tripService.$inject = ['$http',
	                        '$q',
	                        'trip.constants.urls'];
	return tripService;
});