define([], function () {
	'use strict';

	var cityService = function($http, cache, cacheKeys, urls) {

        function _get(cityId) {
			var cCityId = cache.get(cacheKeys.GCITY_ID);
			var cGcity = cache.get(cacheKeys.GCITY);

			var dfd = $.Deferred();
			if (!cCityId || !cGcity || cCityId != cityId){
				cache.put(cacheKeys.GCITY_ID, cityId);
				$http.get(urls.city.replace(':city_q', cityId))
					.success(function (data, status) {
						var gcity = null;
						var gcityName = null;

						if (status == 200) {
							gcity = data.gcity;
							gcityName = data.gcity.name;
						}
						cache.put(cacheKeys.GCITY, gcity);
						cache.put(cacheKeys.GCITY_NAME, gcityName);
						dfd.resolve(gcity);
					})
					.error(function () {
					    cache.remove(cacheKeys.GCITY);
					    cache.remove(cacheKeys.GCITY_ID);
						dfd.resolve(null);
					});
			}
			else{
				dfd.resolve(cGcity);
			}
			return dfd.promise();
		};

		function _getNearParam(cityId) {
		    var p = _get(cityId);
		    var dfd = $.Deferred();
		    p.done(function(gcity) {
		        if (gcity == null) {
		            dfd.resolve("");
                }

                var near;
                if (gcity.fcode == "PPLX") {
                    near = gcity.name + "," + gcity.countryCode;
                }
                else {
                    near = gcity.name + "," + gcity.adminCode1 + "," + gcity.countryCode;
                }
                dfd.resolve(near);
		    });
		    return dfd.promise();
		};

        function _search(cityName) {
            var cCityName = cache.get(cacheKeys.GCITY_NAME);
			var cGcity = cache.get(cacheKeys.GCITY);

			var dfd = $.Deferred();
			if (!cCityName || !cGcity || cCityName != cityName){
				cache.put(cacheKeys.GCITY_NAME, cityName);
				$http.get(urls.city.replace(':city_q', cityName))
					.success(function (data, status) {
					    console.log(status);
						var gcity = null;
						var gcityId = null;

						if (status == 200) {
							gcity = data.gcity;
							gcityId = data.gcity.geonameId;
						}
						cache.put(cacheKeys.GCITY, gcity);
						cache.put(cacheKeys.GCITY_ID, gcityId);
						dfd.resolve(gcity);
					})
					.error(function () {
					    cache.remove(cacheKeys.GCITY);
					    cache.remove(cacheKeys.GCITY_ID);
						dfd.resolve(null);
					});
			}
			else{
				dfd.resolve(cGcity);
			}
			return dfd.promise();
        };

		return{
		    get: _get,
		    getNearParam: _getNearParam,
		    search : _search
		};
	};

	cityService.$inject = ['$http',
	                        'trip.factories.cache',
	                        'trip.constants.cacheKeys',
	                        'trip.constants.urls'];
	return cityService;
});