define([], function () {
	'use strict';
	
	var userService = function($cookies, cookieKeys) {
	    var self = this;
		var _username = "";
		var _firstName = "";
		var _lastName = "";
		var _token = "";
		var _fbToken = null;

		/*
		 * Public Methods
		 */

        self.getUserName = function() { return _username; };
        self.getFirstName = function () { return _firstName; };
		self.getLastName = function () { return _lastName; };
		self.getFBToken = function () { return _fbToken; };
		self.getToken = function () { return _token; };
		self.getCookieFBToken = function () {
		    return $cookies[cookieKeys.token];
		};
		self.setCookieFBToken = function (val) {
		    if (val == null) {
		        delete $cookies[cookieKeys.token];
		    }
		    else {
		        $cookies[cookieKeys.token] = val;
		    }
		};


        self.clear = function() {
            _username = "";
            _firstName = "";
            _lastName = "";
            _token = "";
            _fbToken = null;

            self.setCookieFBToken(_fbToken);
        };

		self.load = function(data) {
		    _username = data.username;
		    _firstName = data.first_name;
		    _lastName = data.last_name;
		    _token = data.token;
		    _fbToken = data.fbToken;

            self.setCookieFBToken(_fbToken);
		};
	};
	
	userService.$inject = ['$cookies', 'trip.constants.cookieKeys'];
	return userService;
});