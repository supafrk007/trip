define([], function () {
	'use strict';

	var userService = function($http, $q, FBService, User, urls) {
	    var self = this;


		/*
		 * Public Methods
		 */

        self.login = function() {
            FBService.login().then(function(response){
				console.log(response);

				if (response.status == "connected") {
					var fb_token = response.authResponse.accessToken;
					var d = { fb_token : fb_token };

					$http.get(urls.users_get.replace(':fb_token', fb_token), d)
						.success(function (data, status) {
						    data.fbToken = fb_token;
							console.log("Loaded User:");
							console.log(data);
							User.load(data);
							_loginDeferred.resolve();
						})
						.error(function (data, status) {
							console.log(data);
							_loginDeferred.reject();
						});
				}
				else {
				    _loginDeferred.reject();
				}
			});
        };

        self.logout = function() {
            User.clear();
            _setUpIsLoggedIn();
        };

       self.register = function() {
			FBService.login().then(function(response){
				console.log(response);

				if (response.status == "connected") {
					var fb_token = response.authResponse.accessToken;
					var d = { fb_token : fb_token };

					$http.post(urls.users, d)
						.success(function (data, status) {
						    data.fbToken = fb_token;
							console.log("Loaded User:");
							console.log(data);
							User.load(data);
							_loginDeferred.resolve();
						})
						.error(function (data, status) {
							console.log(data);
							_loginDeferred.reject();
						});
				}
				else {
				    _loginDeferred.reject();
				}
			});
		};

		self.tryRelogin = function() {
		    var t = User.getCookieFBToken();
		    if (t != null) {
		        var d = { fb_token : t };
		        $http.get(urls.users_get.replace(':fb_token', t), d)
                    .success(function (data, status) {
                        data.fbToken = t;
                        console.log("Loaded User:");
                        console.log(data);
                        User.load(data);
                        _loginDeferred.resolve();
                    })
                    .error(function (data, status) {
                        console.log(data);
                        _loginDeferred.reject();
                    });
		    }
		    else {
		        _loginDeferred.reject();
		    }
		};

        var _loginDeferred = null;
		self.isLoggedIn = null;

        function _setUpIsLoggedIn() {
            _loginDeferred = $q.defer();
		    self.isLoggedIn = _loginDeferred.promise;
        }

		function _init() {
		    _setUpIsLoggedIn();
		}

		_init();
	};

	userService.$inject = ['$http', '$q', 'cor.auth.FBService', 'trip.services.user', 'trip.constants.urls'];
	return userService;
});