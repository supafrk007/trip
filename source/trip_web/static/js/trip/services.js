define([
    'trip/services/authService',
    'trip/services/cityService',
    'trip/services/tokenAuthInterceptor',
    'trip/services/tripService',
	'trip/services/userService',
	'trip/services/venuesService'
], function (
    authService,
    cityService,
    tokenAuthInterceptor,
    tripService,
	userService,
	venuesService
) {
	'use strict';

	return {
	    'trip.services.auth' : authService,
	    'trip.services.city' : cityService,
	    'trip.services.tokenAuthInterceptor' : tokenAuthInterceptor,
	    'trip.services.trip' : tripService,
		'trip.services.user' : userService,
		'trip.services.venues' : venuesService
	};
});