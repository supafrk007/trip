define([
	'trip/factories/cacheFactory'
], function (
	cacheFactory
) {
	'use strict';

	return {
		'trip.factories.cache' : cacheFactory
	};
});