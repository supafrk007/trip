define([], function () {
	'use strict';

	var partitionFilter = function($cacheFactory){
		var arrayCache = $cacheFactory('trip-partition');
        return function(arr, size) {
            if (arr == undefined) {
                return null;
            }

            var parts = [], cachedParts, jsonArr = JSON.stringify(arr);
            for (var i = 0; i < arr.length; i += size) {
                parts.push(arr.slice(i, i + size));
            }
            cachedParts = arrayCache.get(jsonArr);
            if (JSON.stringify(cachedParts) === JSON.stringify(parts)) {
                return cachedParts;
            }
            arrayCache.put(jsonArr, parts);

            return parts;
        };
	};

	partitionFilter.$inject = ['$cacheFactory'];
	return partitionFilter;
});