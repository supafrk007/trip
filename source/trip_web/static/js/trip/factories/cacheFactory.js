define([], function () {
	'use strict';

	var tripCacheFactory = function($cacheFactory) {
		return $cacheFactory('trip-cache');
	};

	tripCacheFactory.$inject = ['$cacheFactory'];
	return tripCacheFactory;
});