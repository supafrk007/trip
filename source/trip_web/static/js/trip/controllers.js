define([
    'trip/controllers/cityController',
	'trip/controllers/homeController',
	'trip/controllers/navController',
	'trip/controllers/tripController'
], function (
    cityController,
	homeController,
	navController,
	tripController
) {
	'use strict';

	return {
	    'trip.controllers.city' : cityController,
		'trip.controllers.home' : homeController,
		'trip.controllers.nav' : navController,
		'trip.controllers.trip' : tripController
	};
});