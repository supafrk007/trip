define([
    'trip/filters/partitionFilter'
], function (
    partitionFilter
) {
	'use strict';

	return {
	    'trpPartition' : partitionFilter
	};
});