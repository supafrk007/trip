define([
    'trip/directives/venuesDirective'
], function (
    venuesDirective
) {
	'use strict';

	return {
	    'trpVenues' : venuesDirective
	};
});