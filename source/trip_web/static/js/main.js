'use strict';

require.config({
	paths: {
		jquery : "//code.jquery.com/jquery-2.1.1.min",
        bootstrap :  "//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min",  
		angular: '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular.min',
		angularRoute: '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular-route.min',
		angularCookies: '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular-cookies.min',
		angularUI: '//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min',
		bootbox: 'https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.3.0/bootbox.min',
		underscore: 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.2/underscore-min'
		
		//ngStorage: '../bower_components/ngstorage/ngStorage.min'
		
		//,
		// angularMocks: '../lib/angular-mocks/angular-mocks',
		// text: '../lib/requirejs-text/text'
	},
	shim: {
		'angular' : {'exports' : 'angular'},
		'angularRoute': ['angular'],
		'angularCookies': ['angular'],
		'angularUI': ['angular'],
		'bootstrap' : { 'deps' :['jquery'] },
		'bootbox' : ['bootstrap', 'jquery']
		
		// ,
		// 'angularMocks': {
			// deps:['angular'],
			// 'exports':'angular.mock'
		// }
	}
});

window.name = "NG_DEFER_BOOTSTRAP!";

require( [
	'angular',
	'angularRoute',
	'angularCookies',
	'jquery',
	'bootstrap',
	'bootbox',
	'app'
], function(ng, ngRoute, ngCookies, jquery, bootstrap, bootbox, app) {
	ng.element().ready(function() {
        ng.resumeBootstrap();
    });
});

/*require(["jquery", "bootstrap", "bootbox"], function(jq, bs, bootbox) {
    bootbox.alert(
        "If you see this alert as a modal, it's working! :)<br/><br/>In the code<br/><pre>require(['jquery', 'bootstrap', 'bootbox'], function(jq, bs, bootbox) { ... </pre> notice the params '<b>jq</b>', '<b>bs</b>' and '<b>bootbox</b>' that are in the same order as the string array.. (first two are dummies in this example :)");
});*/