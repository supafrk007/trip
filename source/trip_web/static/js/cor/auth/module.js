define([
	'angular',
	'cor/auth/services'
], function (
	ng,
	services
) {
	'use strict';
	
	var authModule = ng.module('cor.auth', []);
	//authModule.controller(controllers);
	//authModule.factory(factories);
	authModule.service(services);
	
	return authModule;
});