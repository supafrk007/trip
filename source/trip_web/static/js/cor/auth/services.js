define([
	'cor/auth/services/facebookService'
], function (
	facebookService
) {
	'use strict';
	
	return {
		'cor.auth.FBService' : facebookService
	};
});