define([
	'angular',
	'jquery',
	'bootbox',
], function (
	ng,
	jquery,
	bootbox
) {
	'use strict';

    var bbFactory = function() {
        return bootbox;
    };
    var bbProvider = function() {
        return {
            setDefaults: function (options) {
                bootbox.setDefaults(options);
            },
            $get: function () {
                return {
                }
            }
        }
    };

	var bbModule = ng.module('cor.bootbox', []);
	bbModule.factory('bootbox', bbFactory);
	bbModule.provider('$bootbox', bbProvider);

	return bbModule;
});