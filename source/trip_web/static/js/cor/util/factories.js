define([
	'cor/util/factories/localStorage'
], function (
	localStorage
) {
	'use strict';

	return {
		'cor.util.localStorage' : localStorage
	};
});