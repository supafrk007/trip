define([
	'angular',
	'cor/util/factories',
	'cor/util/services'
], function (
	ng,
	factories,
	services
) {
	'use strict';
	
	var utilModule = ng.module('cor.util', []);
	utilModule.factory(factories);
	utilModule.service(services);
	
	return utilModule;
});