define([], function () {
	'use strict';

	var pagerService = function() {

        function _getPages(curPage, numPages, numToShow) {
            if (numPages <= numToShow) {
                return _.range(1, numPages+1);
            }
            else {
                var half = numToShow / 2;
                var start = 1;
                var stop = 1;

                if (numToShow % 2 == 1) {
                    start = curPage - half;
                    stop = curPage + half;
                }
                else {
                    start = curPage - half;
                    stop = curPage + half - 1;
                }

                if (start <= 0) {
                    return _.range(1, numToShow+1);
                }
                else if (stop >= numPages) {
                    return _.range(numPages - numToShow+1, numPages + 1);
                }
                else {
                    return _.range(start, stop+1);
                }
            }
        }

		return{
		    getPages: _getPages
		};
	};

	pagerService.$inject = [];
	return pagerService;
});