define([
	'cor/util/services/pagerService'
], function (
	pagerService
) {
	'use strict';

	return {
		'cor.util.services.pager' : pagerService
	};
});