define([
    'jquery',
    'underscore',
    'bootstrap',
    'bootbox',
	'angular',
	'angularCookies',
	'angularRoute',
	'angularUI',
	'trip/module'
], function (
    jquery,
    underscore,
    bootstrap,
    bootbox,
	ng,
	ngCookies,
	ngRoute,
	ngUI,
	tripModule
	) {
});