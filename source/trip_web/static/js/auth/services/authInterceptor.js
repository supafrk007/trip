define([], function () {
	'use strict';
	
	var authInterceptor = function ($injector, $location, $q) {
		var self = {
			request: request,
			responseError: responseError
		};
	
	    return self;
	
	    function request(config) {
	    	var Auth = $injector.get('trip.auth.Auth');
	    	var token = Auth.getToken();
	
			if (token) {
				config.headers['Authorization'] = 'JWT ' + token;
			}
			
			return config;
		}
	
	    function responseError(response) {
	    	if (response.status === 403) {
	    		if (response.data.detail === 'Signature has expired.') {
	    			$location.path('/login');
    			}
			}
			return $q.reject(response);
		}
	};
	
	authInterceptor.$inject = ['$injector', '$location', '$q'];  
	return authInterceptor;
});