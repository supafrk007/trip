define([
	'auth/controllers/loginController',
	'auth/controllers/registerController'
], function (
	loginController,
	registerController
) {
	'use strict';

	return {
		'auth.loginController' : loginController,
		'auth.registerController' : registerController
	};
});