define([
	'auth/factories/authFactory',
	'auth/factories/facebookFactory', 
	'auth/factories/usersFactory'
], function (
	authFactory, 
	fbFactory,
	usersFactory
) {
	'use strict';

	return {
		'trip.auth.Auth' : authFactory,
		'trip.auth.FB' : fbFactory,
		'trip.auth.Users' : usersFactory
	};
});