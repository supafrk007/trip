define([
	'auth/services/authInterceptor'
], function (
	authInterceptor
) {
	'use strict';
	
	return {
		'trip.auth.authInterceptor' : authInterceptor
	};
});