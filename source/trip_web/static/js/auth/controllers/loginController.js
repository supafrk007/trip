define(['auth/urls'], function (auth_urls) {
	'use strict';
	
	var loginCtrl = function($scope, $http, Auth, FBAuth){
		var vm = this;

		$scope.register_fb = function() {
			FBAuth.login().then(function(response){
				console.log(response);
				
				if (response.status == "connected") {
					var fb_token = response.authResponse.accessToken;
					var d = { fb_token : fb_token };
					
					$http.post(auth_urls.users, d)
						.success(function (data, status) {
							console.log(data);
						});
				}
			});
		};

		$scope.login_fb = function() {
			FBAuth.login().then(function(response){
				console.log(response);
				
				if (response.status == "connected") {
					var fb_token = response.authResponse.accessToken;
					var d = { fb_token : fb_token };
					
					/*$http.post(auth_urls.users, d)
						.success(function (data, status) {
							
						});*/
				}
			});
		};
	};
	
	loginCtrl.$inject = ['$scope', '$http', 
							'trip.auth.Auth', 
							'trip.auth.FB'];
	
	return loginCtrl;
});