define([], function () {
	'use strict';
	
	var registerCtrl = function(Auth){
		var vm = this;

	    vm.register = register;
	
	    function register() {
	      Auth.register(vm.username, vm.password, vm.email);
	    }
	};
	
	registerCtrl.$inject = ['trip.auth.Auth'];  
	
	return registerCtrl;
});