define([], function () {
	'use strict';
	
	var c_urls = function(){
		var self = this;
		
		self.users = "/api/v1/users/";
	};
	
	var urls = new c_urls();
	
	return urls;
});