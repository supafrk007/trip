define([], function () {
	'use strict';
	
	var authService = function($http, $window) {
		function deleteToken() {
	      $window.localStorage.removeItem('token');
	    }
	
	    function getToken() {
	      return $window.localStorage.getItem('token');
	    }
	
	    /*
	    function login(username, password) {
	      return $http.post('/api/v1/auth/login/', {
	        username: username, password: password
	      }).then(loginSuccessFn, loginErrorFn);
	
	      function loginSuccessFn(data, status, headers, config) {
	        if (data.data.token) {
	          self.setToken(data.data.token);
	        }
	
			//console.log(data.data);
	        $window.location = '/';
	      }
	
	      function loginErrorFn(data, status, headers, config) {
	        console.error(data);
	      }
	    }
	    */
	
	    function logout() {
	      self.deleteToken();
	      $window.location = '/';
	    }

		/*	
	    function register(username, password, email) {
	      return $http.post('/api/v1/users/', {
	        username: username, password: password, email: email
	      }).then(registerSuccessFn);
	
	      function registerSuccessFn(data, status, headers, config) {
	        self.login(username, password);
	      }
	    }
	    */
	
	    function setToken(token) {
	      $window.localStorage.setItem('token', token);
	    }
		
		var self = {
	      deleteToken: deleteToken,
	      getToken: getToken,
	      //login: login,
	      logout: logout,
	      //register: register,
	      setToken: setToken
	    };

    	return self;
	};
	
	authService.$inject = ['$http', '$window'];
	return authService;
});