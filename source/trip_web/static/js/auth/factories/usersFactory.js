define([], function () {
	'use strict';
	
	var userService = function($http, $window) {
		function all() {
	      return $http.get('/api/v1/users/');
	    }
		
		var self = {
	      all: all
	    };

    	return self;
	};
	
	userService.$inject = ['$http'];
	return userService;
});