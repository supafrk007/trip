define([
	'angular',
	'auth/controllers',
	'auth/factories',
	'auth/services'
], function (
	ng,
	controllers,
	factories,
	services
) {
	'use strict';
	
	var authModule = ng.module('trip.auth', []);
	authModule.controller(controllers);
	authModule.factory(factories);
	authModule.service(services);
	
	return authModule;
});