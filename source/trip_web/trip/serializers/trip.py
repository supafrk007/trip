"""Trip Serializers"""

from rest_framework import serializers
from ..models import Trip, TripCity


class TripSerializer(serializers.ModelSerializer):
    """Serializer for a Trip"""

    class Meta:
        model = Trip
        fields = ('id', 'name', 'start_dt', 'end_dt')


class TripCitySerializer(serializers.ModelSerializer):
    """Serializer for a Trip"""

    class Meta:
        model = TripCity
        fields = ('city_id', 'name', 'admin_name', 'country_name')