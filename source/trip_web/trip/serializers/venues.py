"""Venues Serializers"""

from rest_framework import serializers
from ..models import VenueCategory, Venue


class SubVenueCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = VenueCategory
        fields = ('id', 'name', 'plural_name', 'short_name')


class VenueCategorySerializer(serializers.ModelSerializer):
    """Serializer for a VenueCategory"""

    base_category = SubVenueCategorySerializer(required=False)
    parent_category = SubVenueCategorySerializer(required=False)

    def create(self, validated_data):
        return VenueCategory(**validated_data)

    class Meta:
        model = VenueCategory
        fields = ('id', 'name', 'plural_name', 'short_name', 'base_category', 'parent_category')


class VenueSerializer(serializers.ModelSerializer):
    """Serializer for a Venue"""

    cat = VenueCategorySerializer()

    def create(self, validated_data):
        return Venue(**validated_data)

    def update(self, instance, validated_data):
        """Disable update"""
        return None

    class Meta:
        model = Venue