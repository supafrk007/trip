"""Authentication Serializers"""

from rest_framework import serializers
from ..models import TripUser


class TripUserSerializer(serializers.ModelSerializer):
    """Serializer for a TripUser"""

    class Meta:
        model = TripUser
        fields = ('id', 'first_name', 'last_name', 'fb_user')