"""Test Mixins"""

from ..models import TripUser

class TestUserMixin(object):
    """Test User Mixin"""

    def get_user(self, username=None, user_id=None):
        """Get a User Instance"""
        if user_id is None and username is None:
            raise Exception("Can't provide both id and username")
        elif user_id is not None:
            return TripUser.objects.get(id=user_id)
        else:
            return TripUser.objects.get(username=username)