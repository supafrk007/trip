"""Tests for Bookmarks"""

#from django.utils.unittest import skipIf
from django.core.urlresolvers import reverse
from django.test import TestCase
from rest_framework.test import APIClient
from ..services.bookmarks import BookmarksService
from .mixins import TestUserMixin


class BookmarksServiceTests(TestCase):
    """Bookmarks Service Tests"""

    fixtures = ['test_user', 'test_trip']

    def test_get_bookmarks(self):
        """Test Get Venue Bookmarks"""
        svc = BookmarksService()
        q = svc.get_bookmarks(user_id=1,
                              ids=['4c77fb5c2d3ba143355589d0', '4a640409f964a52018c61fe3'])
        self.assertEqual(len(q), 2)

    def test_get_bookmarks_empty(self):
        """Test missing the bookmarks"""
        svc = BookmarksService()
        q = svc.get_bookmarks(user_id=1,
                              ids=['4d014d3ac8bda0936a1975af',
                                   '4d5fdf71291059418d2eee46'])
        self.assertEqual(len(q), 0)

    def test_get_city_bookmarks(self):
        """Test Get City Bookmarks"""
        svc = BookmarksService()
        ids, count = svc.get_city_bookmarks(city_id=5506956,
                                            user_id=1,
                                            section='',
                                            page_num=1,
                                            page_size=10)
        self.assertEqual(count, 6)
        self.assertEqual(len(ids), count)

    def test_get_city_bookmarks_missing(self):
        """Test Get City Bookmarks Missing"""
        svc = BookmarksService()
        ids, count = svc.get_city_bookmarks(city_id=5506957,
                                            user_id=1,
                                            section='',
                                            page_num=1,
                                            page_size=10)
        self.assertEqual(count, 0)
        self.assertEqual(len(ids), count)

    def test_save_bookmark_missing(self):
        """Test saving a bookmark that is missing"""
        svc = BookmarksService()
        b = svc.save_bookmark(user_id=1,
                              venue_id='512bdb48e4b04df75314d1a3',
                              city_id=5506956,
                              is_active=True)
        self.assertIsNotNone(b)
        self.assertTrue(b.is_active)

    def test_save_bookmark_inactivate(self):
        """Test inactivating a bookmark"""
        svc = BookmarksService()
        b = svc.save_bookmark(user_id=1,
                              venue_id='4c77fb5c2d3ba143355589d0',
                              city_id=5506956,
                              is_active=False)
        self.assertIsNotNone(b)
        self.assertFalse(b.is_active)

    def test_save_bookmark_reactivate(self):
        """Test reactivate a bookmark"""
        svc = BookmarksService()
        b = svc.save_bookmark(user_id=1,
                              venue_id='4d5fdf71291059418d2eee46',
                              city_id=5506956,
                              is_active=False)
        self.assertIsNotNone(b)
        self.assertFalse(b.is_active)


class BookmarkDetailTests(TestCase, TestUserMixin):
    """Bookmark Detail View Tests"""

    fixtures = ['test_user', 'test_trip']

    def test_put(self):
        """Test saving a bookmark"""
        d = {'is_active': 'true',
             'city_id': '5506956'}
        user = self.get_user(username="bkhan1")
        client = APIClient(enforce_csrf_checks=False)
        client.force_authenticate(user=user)
        response = client.put(reverse('bookmark', kwargs={'venue_id': '4e593953e4cd875e8eab465c'}), d)
        self.assertEqual(response.status_code, 200)

    def test_put_unauth(self):
        """Test saving a bookmark unauthenticated"""
        d = {'is_active': 'true',
             'city_id': '5506956'}
        client = APIClient(enforce_csrf_checks=False)
        response = client.put(reverse('bookmark', kwargs={'venue_id': '4e593953e4cd875e8eab465c'}), d)
        self.assertEqual(response.status_code, 403)

    def test_put_unknown_venue(self):
        """Test saving a made up venue"""
        d = {'is_active': 'true',
             'city_id': '5506956'}
        user = self.get_user(username="bkhan1")
        client = APIClient(enforce_csrf_checks=False)
        client.force_authenticate(user=user)
        response = client.put(reverse('bookmark', kwargs={'venue_id': '324jklajfs0d09'}), d)
        self.assertEqual(response.status_code, 404)

    def test_put_deactivate_bookmark(self):
        """Test removing a bookmark"""
        d = {'is_active': 'false',
             'city_id': '5506956'}
        user = self.get_user(username="bkhan1")
        client = APIClient(enforce_csrf_checks=False)
        client.force_authenticate(user=user)
        response = client.put(reverse('bookmark', kwargs={'venue_id': '4c77fb5c2d3ba143355589d0'}), d)
        self.assertEqual(response.status_code, 200)