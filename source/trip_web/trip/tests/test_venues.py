"""Tests for Venues"""

from django.core.urlresolvers import reverse
from django.test import TestCase
#from django.utils.unittest import skipIf
from rest_framework.test import APIClient
from ..services.venues import VenuesService
from .mixins import TestUserMixin


class VenuesServiceTests(TestCase):
    """Venues Service Tests"""

    CLIENT_ID = 'H3ZKK051Q4B3AMRW0KGLLTTAZ2H4UONPB3ZPT2WUJA3IV53R'
    SECRET = 'TPMRNFNZOM1MUFF20DRNZDD50UXACRKEZ4XFK1EKFTOIV5GV'

    fixtures = ['test_user', 'test_trip']

    def test_load_categories(self):
        """Test loading venue categories"""
        svc = VenuesService()
        svc.load_categories(client_id=self.CLIENT_ID,
                            secret=self.SECRET)

    def test_explore_new_places(self):
        """Test Exploring Places that haven't been searched yet"""
        svc = VenuesService()
        (total_venues, venues) = svc.explore(city_id=2147714,
                                             near="Sydney, New South Wales, Australia",
                                             section="",
                                             page_num=1,
                                             page_size=15,
                                             client_id=self.CLIENT_ID,
                                             secret=self.SECRET)
        self.assertGreater(total_venues, 0)
        self.assertGreater(len(venues), 0)

    def test_explore_existing_search(self):
        """Test Exploring an existing search"""
        svc = VenuesService()
        (total_venues, venues) = svc.explore(city_id=5506956,
                                             near="Las Vegas, NV",
                                             section=None,
                                             page_num=1,
                                             page_size=15,
                                             client_id=self.CLIENT_ID,
                                             secret=self.SECRET)
        self.assertGreater(total_venues, 0)
        self.assertGreater(len(venues), 0)

    def test_get_venue_category(self):
        """Test getting a venue category"""
        svc = VenuesService()
        c = svc.get_category('4bf58dd8d48988d100941735')
        self.assertIsNotNone(c)

    def test_get_venue_category_fail(self):
        """Test get venue category failure"""
        svc = VenuesService()
        c = svc.get_category('4bf58dd8d48988d100941735')
        self.assertIsNotNone(c)

    def test_get_venues(self):
        """Test returning a list of venues"""
        svc = VenuesService()
        l = svc.get_venues(['4c77fb5c2d3ba143355589d0',
                            '4a640409f964a52018c61fe3',
                            '4d014d3ac8bda0936a1975af',
                            '4d5fdf71291059418d2eee46'])
        self.assertEqual(len(l), 4)

    def test_get_venues_not_found(self):
        """Test getting not found venues"""
        svc = VenuesService()
        l = svc.get_venues(['blah'])
        self.assertEqual(len(l), 0)


class VenuesSerializerTests(TestCase):
    """Venue Serializer Tests"""

    fixtures = ['test_user', 'test_trip']

    def test_serialize(self):
        from ..models import Venue
        from ..serializers.venues import VenueSerializer
        from rest_framework.renderers import JSONRenderer
        v = Venue.objects.all()[:2]
        serializer = VenueSerializer(v, many=True)
        j = JSONRenderer().render(serializer.data)


class VenuesListTests(TestCase, TestUserMixin):
    """Venues List View Tests"""

    fixtures = ['test_user', 'test_trip']

    def test_get_unauthenticated(self):
        """Test getting venues for unauth users"""
        d = {'city_id': "5391812",
             'near': 'San Diego, CA, USA',
             "page_num": "1",
             "page_size": "15"}
        client = APIClient(enforce_csrf_checks=False)
        response = client.get(reverse('venues'), d)
        self.assertEqual(response.status_code, 200)
        j = response.data
        self.assertGreater(j['total_results'], 0)
        self.assertGreater(len(j['venues']), 0)
        self.assertEqual(len(j['bookmark_ids']), 0)
        #http://localhost:8000/api/venues?city_id=2988507&near=&p=1&ps=10&s=

    def test_get_authenticated(self):
        """Test getting venues for auth users"""
        d = {'city_id': "5506956",
             'near': 'Las Vegas, NV',
             "page_num": "1",
             "page_size": "15"}
        user = self.get_user(username="bkhan1")
        client = APIClient(enforce_csrf_checks=False)
        client.force_authenticate(user=user)
        response = client.get(reverse('venues'), d)
        j = response.data
        self.assertIsNotNone(j)
        self.assertGreater(j['total_results'], 0)
        self.assertGreater(len(j['venues']), 0)
        self.assertGreater(len(j['bookmark_ids']), 0)

    def test_get_new_search(self):
        """Test getting venues for a new search"""
        d = {'city_id': "1232",
             'near': 'Las Vegas, NV, USA',
             "page_num": "1",
             "page_size": "15"}
        user = self.get_user(username="bkhan1")
        client = APIClient(enforce_csrf_checks=False)
        client.force_authenticate(user=user)
        response = client.get(reverse('venues'), d)
        j = response.data
        self.assertIsNotNone(j)
        self.assertGreater(j['total_results'], 0)
        self.assertGreater(len(j['venues']), 0)
        self.assertEqual(len(j['bookmark_ids']), 1)