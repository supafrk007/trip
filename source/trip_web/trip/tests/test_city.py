"""Tests for Cities"""

from django.core.urlresolvers import reverse
from django.test import TestCase
from rest_framework.test import APIClient
from ..services.city import CityService
from .mixins import TestUserMixin


class CityServiceTest(TestCase):
    """Test City Services"""

    def test_get_city(self):
        """Test Get City"""
        svc = CityService()
        gcity = svc.get_city(5391811)
        self.assertEqual(gcity['lat'], '32.71533')

    def test_search_city_paris(self):
        """Test search Paris"""
        svc = CityService()
        g = svc.search_city('Paris')
        self.assertIsNotNone(g)
        self.assertEqual(g['countryCode'], 'FR')
        self.assertEqual(g['name'], 'Paris')
        self.assertEqual(g['fcode'], 'PPLC')

    def test_search_city_london(self):
        """Test search London"""
        svc = CityService()
        g = svc.search_city('London')
        self.assertIsNotNone(g)
        self.assertEqual(g['countryCode'], 'GB')
        self.assertEqual(g['name'], 'London')
        self.assertEqual(g['fcode'], 'PPLC')

    def test_search_city_new_york(self):
        """Test search New York"""
        svc = CityService()
        g = svc.search_city('New York')
        self.assertIsNotNone(g)
        self.assertEqual(g['countryCode'], 'US')
        self.assertEqual(g['name'], 'New York')
        self.assertEqual(g['fcode'], 'PPL')

    def test_search_city_not_found(self):
        """Test search Not Found"""
        svc = CityService()
        g = svc.search_city('qersadf')
        self.assertIsNone(g)


class CityBookmarksListTests(TestCase, TestUserMixin):
    """City Bookmarks List View Tests"""

    fixtures = ['test_user', 'test_trip']

    def test_get_bookmarks(self):
        """Test get bookmarks from a city"""
        d = {'s': '',
             'p': '1',
             'ps': '10'}
        user = self.get_user(username='bkhan1')
        client = APIClient(enforce_csrf_checks=False)
        client.force_authenticate(user=user)
        response = client.get(reverse('city_bookmarks', kwargs={'city_id': 5506956}), d)
        self.assertEqual(response.status_code, 200)
        j = response.data
        self.assertGreater(j['total_results'], 0)
        self.assertEqual(len(j['venues']), j['total_results'])
        self.assertEqual(len(j['bookmark_ids']), len(j['venues']))


class CityDetailTests(TestCase):
    """City Detail View Tests"""

    def test_get_by_id(self):
        """Test get City By Id"""
        client = APIClient(enforce_csrf_checks=False)
        response = client.get(reverse('city', kwargs={'city_q': 5391811}))
        self.assertEqual(response.status_code, 200)
        j = response.data
        self.assertEqual(j['gcity']['lat'], '32.71533')

    def test_get_invalid_city_by_id(self):
        """Test get with an invalid city id"""
        client = APIClient(enforce_csrf_checks=False)
        response = client.get(reverse('city', kwargs={'city_q': 0}))
        self.assertEqual(response.status_code, 200)
        j = response.data
        self.assertIsNone(j['gcity'])

    def test_get_by_name(self):
        """Test get City By Name"""
        client = APIClient(enforce_csrf_checks=False)
        response = client.get(reverse('city', kwargs={'city_q': "London"}))
        self.assertEqual(response.status_code, 200)
        j = response.data
        self.assertEqual(j['gcity']['name'], 'London')

    def test_get_by_invalid_name(self):
        """Test get City By Invalid Name"""
        client = APIClient(enforce_csrf_checks=False)
        response = client.get(reverse('city', kwargs={'city_q': "asfjdlaksdjf"}))
        self.assertEqual(response.status_code, 200)
        j = response.data
        self.assertIsNone(j['gcity'])


class CitiesListTests(TestCase):
    """Cities List View Tests"""

    def test_get(self):
        """Test searching cities"""
        client = APIClient(enforce_csrf_checks=False)
        response = client.get(reverse('cities'), {'q': 'Paris'})
        self.assertEqual(response.status_code, 200)
        j = response.data
        self.assertEqual(j['gcity']['name'], 'Paris')

    def test_get_no_q(self):
        """Test searching cities"""
        client = APIClient(enforce_csrf_checks=False)
        response = client.get(reverse('cities'))
        self.assertEqual(response.status_code, 200)
        j = response.data
        self.assertIsNone(j['gcity'])
