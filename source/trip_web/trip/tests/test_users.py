"""Tests for Users"""
# /trip/tests/test_users.py
from cor.utils.exceptions import CodedError
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils.unittest import skipIf
from rest_framework.test import APIClient
from ..services import users as users_svc


TEST_TOKEN = "CAAC0XLfZBGU8BADGuqycFBuqp5ImjMAQjuSfKiwAQdv11WmrmiI9xFB3gTVdpNr6xDLAYUiHJPCBRowTJ04a3YcjzpNqZCR4xZBbqwtxEZArdrUt4SveV40EN3k7JBGxKQ12k7dRHyDia3r94ri15tetTjZBG4t3q2pD0rLEQ5tRfvOZAZBy8gC8iqfR1uukXZBFIo1IB8ZBZBgNMGt5cAfDfP"


@skipIf(True, "Skip FB Tests")
class FBUserServiceTests(TestCase):
    """Test FB User Register Service"""
    
    def test_register(self):
        """Test Register Method"""
        svc = users_svc.FBUserService(token=TEST_TOKEN)
        (user, token) = svc.register()
        self.assertIsNotNone(user)
        self.assertIsNotNone(user.id)
        self.assertIsNotNone(user.username)
        self.assertIsNotNone(user.first_name)
        self.assertIsNotNone(token)
        self.assertGreater(len(token), 10)
        
    def test_register_should_fail(self):
        """Test Register Method Failure"""
        svc = users_svc.FBUserService(token="BadToken")
        with self.assertRaises(CodedError) as ce:
            svc.register()
        self.assertEqual(ce.exception.code, 1001)
        

@skipIf(True, "Skip FB Tests")        
class UserDetailTests(TestCase):
    """Test UserDetail API"""
     
    def test_post(self):
        """Test Post User"""
        client = APIClient(enforce_csrf_checks=False)
        response = client.post(reverse('users'), {'fb_token': TEST_TOKEN})
        self.assertEqual(response.status_code, 201)
        j = response.data
        self.assertGreater(len(j['username']), 0)
        self.assertGreater(len(j['first_name']), 0)
        self.assertGreater(len(j['last_name']), 0)
        self.assertGreater(len(j['token']), 0)
        
    def test_post_fail(self):
        """Test Post User Fail"""
        client = APIClient(enforce_csrf_checks=False)
        response = client.post(reverse('users'), {'fb_token': "BadToken"})
        self.assertEqual(response.status_code, 400)