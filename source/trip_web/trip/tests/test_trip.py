"""Tests for Trip"""

from django.core.urlresolvers import reverse
from django.test import TestCase
#from django.utils.unittest import skipIf
from rest_framework.test import APIClient
from ..services.trip import TripService
from .mixins import TestUserMixin


class TripServiceTests(TestCase, TestUserMixin):
    """Trip Service Tests"""

    fixtures = ['test_user', 'test_trip']

    def test_add_city(self):
        """Test Add Members"""
        user = self.get_user(username="bkhan1")
        svc = TripService()
        status, city = svc.add_city(trip_id=1,
                                    city_id=5391812,
                                    city_name='San Diego',
                                    city_admin_name='California',
                                    country_name='USA',
                                    user_id=user.id)
        self.assertEqual(status, 200)
        self.assertEqual(city.city_id, 5391812)

    def test_add_city_no_access(self):
        """Test Add Members"""
        svc = TripService()
        status, city = svc.add_city(trip_id=1,
                                    city_id=5391812,
                                    city_name='San Diego',
                                    city_admin_name='California',
                                    country_name='USA',
                                    user_id=100)
        self.assertEqual(status, 401)

    def test_add_members(self):
        """Test Add Members"""
        user = self.get_user(username="mkhan")
        user_add = self.get_user(username="bkhan1")
        svc = TripService()
        status, members = svc.add_members(trip_id=3,
                                          member_ids=[user_add.id],
                                          user_id=user.id)
        self.assertEqual(status, 200)
        self.assertEqual(members[0].member_id, user_add.id)

    def test_add_members_no_access(self):
        """Test Add Members"""
        user = self.get_user(username="bkhan1")
        user_add = self.get_user(username="mkhan")
        svc = TripService()
        status, members = svc.add_members(trip_id=3,
                                          member_ids=[user_add.id],
                                          user_id=user.id)
        self.assertEqual(status, 401)

    def test_create_trip(self):
        """Test Create Trip"""
        user = self.get_user(username="bkhan1")
        svc = TripService()
        t = svc.create_trip(name='Test Trip',
                            user_id=user.id,
                            city_id=5391812,
                            city_name='San Diego',
                            city_admin_name='California',
                            country_name='USA')
        self.assertIsNotNone(t)
        self.assertEqual(t.name, 'Test Trip')

    def test_get_cities(self):
        """Test Get Cities"""
        user = self.get_user(username="bkhan1")
        svc = TripService()
        c = svc.get_cities(trip_id=4, user_id=user.id)
        self.assertIsNotNone(c)
        self.assertEqual(c[0], 200)
        self.assertEqual(len(c[1]), 1)

    def test_get_cities_no_access(self):
        """Test Get Cities"""
        svc = TripService()
        c = svc.get_cities(trip_id=4, user_id=100)
        self.assertIsNotNone(c)
        self.assertEqual(c[0], 401)

    def test_get_members(self):
        """Test Get Members"""
        user = self.get_user(username="bkhan1")
        svc = TripService()
        m = svc.get_members(trip_id=4, active=True, user_id=user.id)
        self.assertIsNotNone(m)
        self.assertEqual(m[0], 200)
        self.assertEqual(len(m[1]), 2)

    def test_get_members_no_access(self):
        """Test Get Members"""
        svc = TripService()
        m = svc.get_members(trip_id=4, active=True, user_id=100)
        self.assertIsNotNone(m)
        self.assertEqual(m[0], 401)

    def test_get_trip_success(self):
        """Test Get Trip Success"""
        svc = TripService()
        t = svc.get_trip(1, 1)
        self.assertIsNotNone(t)
        self.assertEqual(t[0], 200)
        self.assertGreater(len(t[1].name), 0)

    def test_get_trip_not_found(self):
        """Test Get Fail"""
        svc = TripService()
        t = svc.get_trip(100, 1)
        self.assertIsNotNone(t)
        self.assertEqual(t[0], 404)
        self.assertIsNone(t[1])

    def test_get_trip_not_member(self):
        """Test user is not a member"""
        svc = TripService()
        t = svc.get_trip(1, 100)
        self.assertIsNotNone(t)
        self.assertEqual(t[0], 401)
        self.assertIsNone(t[1])


class TripSerializerTests(TestCase):
    """Trip Serializer Tests"""

    fixtures = ['test_user', 'test_trip']

    def test_serialize_trip(self):
        from ..models import Trip
        from ..serializers.trip import TripSerializer
        from rest_framework.renderers import JSONRenderer
        t = Trip.objects.all()[0]
        serializer = TripSerializer(t)
        JSONRenderer().render(serializer.data)

    def test_serialize_city(self):
        from ..models import TripCity
        from ..serializers.trip import TripCitySerializer
        from rest_framework.renderers import JSONRenderer
        c = TripCity.objects.all()[0]
        serializer = TripCitySerializer(c)
        JSONRenderer().render(serializer.data)

    def test_serialize_user(self):
        from ..models import TripUser
        from ..serializers.auth import TripUserSerializer
        from rest_framework.renderers import JSONRenderer
        u = TripUser.objects.all()[0]
        serializer = TripUserSerializer(u)
        print(JSONRenderer().render(serializer.data))


class TripCreatorTests(TestCase, TestUserMixin):
    """Trip Creator View Tests"""

    fixtures = ['test_user', 'test_trip']

    def test_post(self):
        """Test Post Success"""
        d = {'city_id': '5506956',
             'name': 'Bilals Trip'}
        user = self.get_user(username="bkhan1")
        client = APIClient(enforce_csrf_checks=False)
        client.force_authenticate(user=user)
        response = client.post(reverse('trip_create'), d)
        self.assertEqual(response.status_code, 200)
        j = response.data
        self.assertGreater(j['id'], 0)
        self.assertGreater(len(j['name']), 0)

    def test_post_no_access(self):
        """Test Post No Access Failure"""
        d = {'city_id': '5506956',
             'name': 'Bilals Trip'}
        client = APIClient(enforce_csrf_checks=False)
        response = client.post(reverse('trip_create'), d)
        self.assertEqual(response.status_code, 403)

    def test_post_bad_city(self):
        """Test Post Bad City Failure"""
        d = {'city_id': '0',
             'name': 'Bilals Trip'}
        user = self.get_user(username="bkhan1")
        client = APIClient(enforce_csrf_checks=False)
        client.force_authenticate(user=user)
        response = client.post(reverse('trip_create'), d)
        self.assertEqual(response.status_code, 500)


class TripDetailTests(TestCase, TestUserMixin):
    """Trip Detail View Tests"""

    fixtures = ['test_user', 'test_trip']

    def test_get(self):
        """Test Get Success"""
        user = self.get_user(username="bkhan1")
        client = APIClient(enforce_csrf_checks=False)
        client.force_authenticate(user=user)
        response = client.get(reverse('trip', kwargs={'trip_id': 1}))
        self.assertEqual(response.status_code, 200)
        j = response.data
        self.assertGreater(j['trip']['id'], 0)
        self.assertGreater(len(j['trip']['name']), 0)
        self.assertGreater(len(j['cities']), 0)
        self.assertGreater(len(j['members']), 0)

    def test_get_no_access(self):
        """Test Try Get wihtout Access"""
        client = APIClient(enforce_csrf_checks=False)
        response = client.get(reverse('trip', kwargs={'trip_id': 1}))
        self.assertEqual(response.status_code, 403)

    def test_get_not_member(self):
        """Test Try to get Trip without being a member"""
        user = self.get_user(username="mkhan")
        client = APIClient(enforce_csrf_checks=False)
        client.force_authenticate(user=user)
        response = client.get(reverse('trip', kwargs={'trip_id': 1}))
        self.assertEqual(response.status_code, 401)