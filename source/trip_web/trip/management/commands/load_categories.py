from django.conf import settings
from django.core.management.base import BaseCommand
from ...services.venues import VenuesService

class Command(BaseCommand):
    """Load Categories Command"""

    def handle(self, *args, **options):
        """Handle Command"""
        svc = VenuesService()
        svc.load_categories(client_id=settings.FSQ_CLIENT_ID,
                            secret=settings.FSQ_SECRET)