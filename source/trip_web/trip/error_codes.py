#===============================================================================
# Authentication Errors : 1000 
#===============================================================================

DUP_USER_REG = (1000, '%s has already registered')
INVALID_FB_TOKEN = (1001, 'Unable to authenticate with Facebook')