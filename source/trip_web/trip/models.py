# /trip/models.py
from django.db import models
from django.contrib.auth.models import AbstractUser
from cor.social.facebook.models import FBUser
import json


#===============================================================================
# User Models
#===============================================================================

class TripUser(AbstractUser):
    """Trip User"""
    
    fb_user = models.OneToOneField(FBUser, unique=True, blank=True, null=True)
    
    @property
    def first_last_name(self):
        """Return full name"""
        return self.first_name + ' ' + self.last_name
    
    def __unicode__(self):
        """String Representation"""
        return self.username
    
    class Meta:
        """Meta Info"""
        db_table = 'trip_user'


#===============================================================================
# Venue Models
#===============================================================================

class VenueCategory(models.Model):
    """A Category for a Venue"""

    id = models.CharField('Foursquare Category ID', max_length=36, primary_key=True)
    base_category = models.ForeignKey('VenueCategory', blank=True, null=True, related_name='+')
    parent_category = models.ForeignKey('VenueCategory', blank=True, null=True, related_name='+')
    name = models.CharField('Name', max_length=50)
    plural_name = models.CharField('Plural Name', max_length=52)
    short_name = models.CharField('Short Name', max_length=52)
    create_dt = models.DateTimeField('Create Date', auto_now_add=True)
    update_dt = models.DateTimeField('Update Date', auto_now=True)

    def __unicode__(self):
        """String representation"""
        return self.name

    class Meta:
        """Meta Information"""
        db_table = 'venue_category'


class Venue(models.Model):
    """A Venue"""

    id = models.CharField('Foursquare Venue ID', max_length=36, primary_key=True)
    name = models.CharField('Name', max_length=200)
    cat = models.ForeignKey(VenueCategory)
    lat = models.DecimalField('Latitude', max_digits=17, decimal_places=14)
    lng = models.DecimalField('Longitude', max_digits=17, decimal_places=14)
    rating = models.DecimalField('Rating', max_digits=4, decimal_places=2, blank=True, null=True)
    contact = models.CharField('JSON Contact Info', max_length=500)
    location = models.CharField('JSON Location', max_length=500)
    photo_url_prefix = models.CharField('Photo URL prefix', max_length=75, blank=True, null=True)
    photo_url_suffix = models.CharField('Photo URL suffix', max_length=75, blank=True, null=True)
    url = models.URLField('Site URL', blank=True, null=True)
    tip_text = models.TextField('Tip Text', blank=True, null=True)
    create_dt = models.DateTimeField('Create Date', auto_now_add=True)
    update_dt = models.DateTimeField('Update Date', auto_now=True)

    def __unicode__(self):
        """String representation"""
        return self.name

    class Meta:
        """Meta Information"""
        db_table = 'venue'


class VenueSearch(models.Model):
    """A search for venues"""

    city_id = models.IntegerField('City Id')
    q = models.CharField('Query Representation', max_length=100)
    page_num = models.SmallIntegerField('Page Number')
    total_venues = models.SmallIntegerField('Total Venues')
    create_dt = models.DateTimeField('Create Date', auto_now_add=True)
    update_dt = models.DateTimeField('Update Date', auto_now=True)

    def __unicode__(self):
        """String representation"""
        return self.q

    class Meta:
        """Meta Information"""
        db_table = 'venue_search'
        unique_together = ('city_id', 'q', 'page_num')


class VenueSearchMap(models.Model):
    """A Map of venues from a search"""

    search = models.ForeignKey(VenueSearch)
    venue = models.ForeignKey(Venue)
    sort_order = models.SmallIntegerField('Sort Order')
    create_dt = models.DateTimeField('Create Date', auto_now_add=True)
    update_dt = models.DateTimeField('Update Date', auto_now=True)

    def __unicode__(self):
        """String representation"""
        return self.search

    class Meta:
        """Meta Information"""
        db_table = 'venue_search_map'
        unique_together = ('search', 'venue')


class VenueBookmark(models.Model):
    """A Bookmark for a venue"""

    user = models.ForeignKey(TripUser)
    venue = models.ForeignKey(Venue)
    city_id = models.IntegerField('City Id', db_index=True)
    is_active = models.BooleanField('Is Bookmark Active', default=True)
    create_dt = models.DateTimeField('Create Date', auto_now_add=True)
    update_dt = models.DateTimeField('Update Date', auto_now=True)

    def __unicode__(self):
        """String representation"""
        return self.venue

    class Meta:
        """Meta Information"""
        db_table = 'venue_bookmark'


#===============================================================================
# Trip
#===============================================================================

class Trip(models.Model):
    """A trip"""

    name = models.CharField('Trip Name', max_length=250)
    start_dt = models.DateTimeField('Start Date', blank=True, null=True)
    end_dt = models.DateTimeField('End Date', blank=True, null=True)
    create_user = models.ForeignKey(TripUser, related_name='+')
    create_dt = models.DateTimeField('Create Date', auto_now_add=True)
    update_user = models.ForeignKey(TripUser, blank=True, null=True, related_name='+')
    update_dt = models.DateTimeField('Update Date', auto_now=True)

    def __unicode__(self):
        """Strip representation"""
        return self.name

    class Meta:
        """Meta Information"""
        db_table = "trip"


class TripCity(models.Model):
    """A City in a trip"""

    trip = models.ForeignKey(Trip)
    city_id = models.IntegerField('City Id', db_index=True)
    name = models.CharField('City Name', max_length=250)
    admin_name = models.CharField('Admin Name', max_length=250)
    country_name = models.CharField('Country Name', max_length=250)
    is_active = models.BooleanField('Is Bookmark Active', default=True)
    create_user = models.ForeignKey(TripUser, related_name='+')
    create_dt = models.DateTimeField('Create Date', auto_now_add=True)
    update_user = models.ForeignKey(TripUser, blank=True, null=True, related_name='+')
    update_dt = models.DateTimeField('Update Date', auto_now=True)

    def __unicode__(self):
        """String representation"""
        return self.city_id

    class Meta:
        """Meta Information"""
        db_table = 'trip_city'


class TripMember(models.Model):
    """A Trip Member"""

    trip = models.ForeignKey(Trip)
    member = models.ForeignKey(TripUser, related_query_name='trip_members')
    is_active = models.BooleanField('Is Bookmark Active', default=True)
    create_user = models.ForeignKey(TripUser, related_name='+')
    create_dt = models.DateTimeField('Create Date', auto_now_add=True)
    update_user = models.ForeignKey(TripUser, blank=True, null=True, related_name='+')
    update_dt = models.DateTimeField('Update Date', auto_now=True)

    def __unicode__(self):
        """String Representation"""
        return self.member

    class Meta:
        """Meta Information"""
        db_table = 'trip_member'


class TripAppointment(models.Model):
    """A venue appointment on a trip"""

    trip = models.ForeignKey(Trip, related_name='trip_appointments')
    venue = models.ForeignKey(Venue)
    start_dt = models.DateTimeField('Start Date')
    end_dt = models.DateTimeField('End Date', blank=True, null=True)
    is_active = models.BooleanField('Is Appointment Active', default=True)
    is_all_day = models.BooleanField('Is All Day Appt', default=True)
    create_user = models.ForeignKey(TripUser, related_name='+')
    create_dt = models.DateTimeField('Create Date', auto_now_add=True)
    update_user = models.ForeignKey(TripUser, blank=True, null=True, related_name='+')
    update_dt = models.DateTimeField('Update Date', auto_now=True)

    def __unicode__(self):
        """String Representation"""
        return self.venue

    class Meta:
        """Meta Information"""
        db_table = 'trip_appointment'