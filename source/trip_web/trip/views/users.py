"""User Views"""

from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from cor.utils.exceptions import CodedError
from ..services.users import FBUserService

import logging
log = logging.getLogger(__name__)


class UserDetail(APIView):
    """User Detail View"""
    permission_classes = (AllowAny,)
    
    def get(self, request):
        """
        Get a User
        Returns: user and site token
        """
        log.debug('Enter UserDetail.get')
        fb_token = request.query_params['fb_token']
        
        try:
            svc = FBUserService(token=fb_token)
            (user, token) = svc.login()
            j = self._serialized_user_token(user, token)
            return Response(j, status=status.HTTP_200_OK)
        except CodedError as e:
            return Response(e.error_dict, status.HTTP_400_BAD_REQUEST)
            
    def post(self, request):
        """
        Create a User
        Returns: user and site token
        """
        log.debug('Enter UserDetail.post')
        fb_token = request.data['fb_token']
        
        try:
            svc = FBUserService(token=fb_token)
            (user, token) = svc.register()
            j = self._serialized_user_token(user, token)
            return Response(j, status=status.HTTP_201_CREATED)
        except CodedError as e:
            return Response(e.error_dict, status.HTTP_400_BAD_REQUEST)
        
    def _serialized_user_token(self, user, token):
        """Serialize user and token"""
        c_user = {'username': user.username,
                  'first_name': user.first_name,
                  'last_name': user.last_name,
                  'token': token
                  }
        return c_user


# import datetime
# from django.utils.timezone import utc
# from rest_framework.authtoken.views import ObtainAuthToken
# 
# class ObtainExpiringAuthToken(ObtainAuthToken):
#     def post(self, request):
#         serializer = self.serializer_class(data=request.DATA)
#         if serializer.is_valid():
#             token, created =  Token.objects.get_or_create(user=serializer.object['user'])
# 
#             if not created:
#                 # update the created time of the token to keep it valid
#                 token.created = datetime.datetime.utcnow().replace(tzinfo=utc)
#                 token.save()
# 
#             return Response({'token': token.key})
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)