"""Trip Views"""

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from ..serializers.auth import TripUserSerializer
from ..serializers.trip import TripSerializer, TripCitySerializer
from ..services.city import CityService
from ..services.trip import TripService

import logging
log = logging.getLogger(__name__)


class TripCreator(APIView):
    """Trip Creator View"""
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        """Post a new Trip"""
        log.debug('Enter TripDetail.post')
        log.debug(request.data)

        name = request.data['name']
        city_id = int(request.data['city_id'])

        city_svc = CityService()
        trip_svc = TripService()

        c = city_svc.get_city(city_id)
        if c is None:
            log.error('City %s not found' % city_id)
            return Response({}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        city_name = c['name']
        city_admin_name = c['adminName1']
        country_name = c['countryName']

        trip = trip_svc.create_trip(name=name,
                                    user_id=request.user.id,
                                    city_id=city_id,
                                    city_name=city_name,
                                    city_admin_name=city_admin_name,
                                    country_name=country_name)

        trip_serializer = TripSerializer(trip)
        j = trip_serializer.data
        return Response(j, status=status.HTTP_200_OK)


class TripDetail(APIView):
    """Trip Detail View"""
    permission_classes = (IsAuthenticated,)

    def get(self, request, trip_id):
        """Get a trip"""
        log.debug('Enter TripDetail.get(%s)' % trip_id)

        svc = TripService()
        user_id = request.user.id

        status, trip = svc.get_trip(trip_id=trip_id, user_id=user_id)
        if status != 200:
            return Response({}, status=status)

        status, cities = svc.get_cities(trip_id=trip_id, user_id=user_id, check_security=False)
        if status != 200:
            return Response({}, status=status)

        status, members = svc.get_members(trip_id=trip_id, user_id=user_id, check_security=False)
        if status != 200:
            return Response({}, status=status)


        ser_trip = TripSerializer(trip)
        ser_city = TripCitySerializer(cities, many=True)
        ser_member = TripUserSerializer(members, many=True)

        j = {'trip': ser_trip.data,
             'cities': ser_city.data,
             'members': ser_member.data}

        return Response(j, status=status)