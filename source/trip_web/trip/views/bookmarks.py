"""Bookmarks"""

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from ..services.bookmarks import BookmarksService

import logging
log = logging.getLogger(__name__)


class BookmarkDetail(APIView):
    """Venue Bookmark Detail View"""
    permission_classes = (IsAuthenticated,)

    def put(self, request, venue_id):
        """Set a bookmark"""
        log.debug('Enter VenueBookmarkDetail.put')
        log.debug(request.data)
        is_active = bool(request.data['is_active'])
        city_id = int(request.data['city_id'])

        svc = BookmarksService()
        b = svc.save_bookmark(user_id=request.user.id,
                              venue_id=venue_id,
                              city_id=city_id,
                              is_active=is_active)

        if b is None:
            return Response({}, status.HTTP_404_NOT_FOUND)
        elif is_active != b.is_active:
            return Response({}, status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({}, status=status.HTTP_200_OK)