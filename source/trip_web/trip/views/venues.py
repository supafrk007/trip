"""Venues Views"""

from django.conf import settings
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from ..services.bookmarks import BookmarksService
from ..services.venues import VenuesService
from ..serializers.venues import VenueSerializer

import logging
log = logging.getLogger(__name__)


class VenuesList(APIView):
    """Venues List View"""
    permission_classes = (AllowAny,)

    def get(self, request):
        """Get a list of venues"""
        log.debug('Enter VenuesList.get')

        client_id = settings.FSQ_CLIENT_ID
        secret = settings.FSQ_SECRET

        city_id = int(request.query_params['city_id'])
        near = request.query_params['near']
        section = request.query_params.get('s', None)
        page_num = int(request.query_params.get('p', '1'))
        page_size = int(request.query_params.get('ps', '10'))

        load_bookmarks = request.user.is_authenticated()
        user_id = request.user.id if request.user.is_authenticated() else None

        if section == '':
            section = None

        svc = VenuesService()
        (total_venues, venues) = svc.explore(city_id=city_id,
                                             near=near,
                                             section=section,
                                             page_num=page_num,
                                             page_size=page_size,
                                             user_id=user_id,
                                             client_id=client_id,
                                             secret=secret)
        if load_bookmarks:
            b_svc = BookmarksService()
            venue_ids = [v.id for v in venues]
            bookmarks = b_svc.get_bookmarks(user_id=user_id, ids=venue_ids)
            bookmark_ids = [b.venue_id for b in bookmarks]
        else:
            bookmark_ids = []

        serializer = VenueSerializer(venues, many=True)

        j = {'total_results': total_venues,
             'venues': serializer.data,
             'bookmark_ids': bookmark_ids}
        return Response(j, status=status.HTTP_200_OK)