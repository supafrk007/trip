"""City Views"""

from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from ..serializers.venues import VenueSerializer
from ..services.bookmarks import BookmarksService
from ..services.city import CityService
from ..services.venues import VenuesService

import logging
log = logging.getLogger(__name__)


class CityBookmarkList(APIView):
    """List of bookmarks in a city"""
    permission_classes = (IsAuthenticated,)

    def get(self, request, city_id):
        """Get a list of bookmarks in a city"""
        log.debug('Enter CityBookmarkList.get(%s)' % city_id if city_id is not None else '')

        section = request.query_params.get('s', None)
        page_num = int(request.query_params.get('p', '1'))
        page_size = int(request.query_params.get('ps', '10'))

        if section == '':
            section = None

        b_svc = BookmarksService()
        v_svc = VenuesService()
        venue_ids, total_count = b_svc.get_city_bookmarks(city_id=city_id,
                                                          user_id=request.user.id,
                                                          section=section,
                                                          page_num=page_num,
                                                          page_size=page_size)
        venues = v_svc.get_venues(venue_ids)
        bookmark_ids = [v.id for v in venues]

        serializer = VenueSerializer(venues, many=True)
        j = {'total_results': total_count,
             'venues': serializer.data,
             'bookmark_ids': bookmark_ids}

        return Response(j, status=status.HTTP_200_OK)


class CityDetail(APIView):
    """City Detail View"""
    permission_classes = (AllowAny,)

    def get(self, request, city_q):
        """Get a City"""
        log.debug('Enter CityDetail.get(%s)' % city_q if city_q is not None else '')

        svc = CityService()
        gcity = svc.get_city(city_q) if city_q.isdigit() else svc.search_city(city_q)
        j = {'gcity': gcity}
        return Response(j, status=status.HTTP_200_OK)


class CitiesList(APIView):
    """Cities List View"""
    permission_classes = (AllowAny,)

    def get(self, request):
        """Search a list of Cities"""
        q = request.query_params.get('q', None)
        log.debug('Enter CitiesList.get(%s)' % q)
        svc = CityService()
        gcity = svc.search_city(q)
        j = {'gcity': gcity}
        return Response(j, status=status.HTTP_200_OK)