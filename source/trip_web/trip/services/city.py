"""City Services"""

from django.conf import settings
import logging
log = logging.getLogger(__name__)

class CityService(object):
    """City Service"""

    #===============================================================================
    # Public Methods
    #===============================================================================

    #def get_bookmarks(self, geonameId, city_id, section, page_num, page_size):
    #    """Get bookmarks for a city"""
    #    pass

    def get_city(self, geonameId):
        """Get a city by geonameId"""

        from cor.geo.geonames import GeonamesHelper
        h = GeonamesHelper(settings.GEONAMES_USERNAME)
        params = {'geonameId': geonameId}
        return h.get(params)

    def search_city(self, q):
        """Search for a one city match"""

        r = self._search_cities(q, True)
        if 'totalResultsCount' not in r:
            return None
        else:
            return None if r['totalResultsCount'] == 0 else r['geonames'][0]

    def search_cities(self, q):
        """Search for cities matching the q"""

        r = self._search_cities(q, False)
        return None if r['totalResultsCount'] == 0 else r['geonames'][0]

    #===============================================================================
    # Private Methods
    #===============================================================================

    def _search_cities(self, q, only_first):
        """Search Geonames for cities matching the q"""

        from cor.geo.geonames import GeonamesHelper
        h = GeonamesHelper(settings.GEONAMES_USERNAME)
        params = {'q': q,
                  'isNameRequired': 'true',
                  'featureClass': 'P'}

        return h.search_first(params) \
            if only_first == True \
            else h.search(params)