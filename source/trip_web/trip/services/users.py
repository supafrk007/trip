"""User Services"""
import datetime
from cor.utils.exceptions import CodedError
from cor.social.facebook.models import FBUser
from rest_framework.authtoken.models import Token
from .. import error_codes, models


class FBUserService(object):
    """FB User Service"""
    
    def __init__(self, token, cls_fb_user=None):
        """Constructor"""
        self._cls_fb_user = FBUser if cls_fb_user is None else cls_fb_user
        self._token = token

    def login(self):
        """Login a FB User"""
        fb_user = self._get_fb_user()
        user = self._get_user(fb_user_id=fb_user.id)
        token = self._create_token(user, True)
        return (user, token.key)

    def register(self):
        """Register a FB User"""
        from cor.utils.rand import rand_hex
        
        fb_user = self._get_fb_user()       
        password = rand_hex(8)
        user = self._create_user(username=fb_user.username, 
                                 email=fb_user.email, 
                                 password=password, 
                                 first_name=fb_user.first_name, 
                                 last_name=fb_user.last_name, 
                                 fb_user_id=fb_user.id)
        token = self._create_token(user)
        return (user, token.key)
    
    def _create_token(self, user, recreate=False):
        """Create a token"""
        import pytz
        token, created = Token.objects.get_or_create(user=user)

        if recreate:
            # recreate if token has expired        
            utc_now = datetime.datetime.utcnow()
            utc_now = utc_now.replace(tzinfo=pytz.utc)
            if not created and token.created < utc_now - datetime.timedelta(hours=24):
                token.delete()
                token = Token.objects.create(user=user)
                token.created = datetime.datetime.utcnow()
                token.save()
        
        return token
    
    def _create_user(self,
                     username,
                     email,
                     password,
                     first_name,
                     last_name,
                     fb_user_id):
        """Create a user"""
        
        from django.db.utils import IntegrityError
        
        try:
            user = models.TripUser.objects.create(username=username,
                                                  password=password,
                                                  email=email,
                                                  first_name=first_name,
                                                  last_name=last_name,
                                                  fb_user_id=fb_user_id)
        except IntegrityError:
            raise CodedError(error_codes.DUP_USER_REG[0],
                             error_codes.DUP_USER_REG[1] % username)
        
        return user
    
    def _get_fb_user(self):
        """Get a fb user"""
        from urllib.error import HTTPError
        
        try:
            fb_user = self._cls_fb_user.load(token=self._token, secret=self._token)
        except HTTPError as e:
            fb_user = None
            
        if fb_user is None:
            err = error_codes.INVALID_FB_TOKEN
            raise CodedError(err[0], err[1])
            
        return fb_user
    
    def _get_user(self, fb_user_id):
        """Get a user with a particular fb id"""
        from django.contrib.auth import get_user_model
        User = get_user_model()
        u = User.objects.get(fb_user_id=fb_user_id)
        return u