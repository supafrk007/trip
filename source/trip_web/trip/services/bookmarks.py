"""Venue Bookmarks"""

from .. import models
from ..constants import foursquare as fs_consts

import logging
log = logging.getLogger(__name__)


class BookmarksService(object):
    """Bookmarks Service"""

    # ===============================================================================
    # Public Methods
    # ===============================================================================

    def get_bookmarks(self, user_id, ids):
        """Get bookmarks for the given user matching the ids"""
        q = models.VenueBookmark.objects.filter(user_id=user_id)
        q = q.filter(is_active=True)
        q = q.filter(venue_id__in=ids)
        return q[:]

    def get_city_bookmarks(self, city_id, user_id, section, page_num, page_size):
        """Get bookmarks for given city"""
        from cor.db import exec_sql
        sql = """SELECT v.id
                FROM venue_bookmark vb
                INNER JOIN venue v on vb.venue_id = v.id
                INNER JOIN venue_category vc on vc.id = v.cat_id
                WHERE
                    vb.city_id = %s
                    AND vb.user_id = %s
                    AND vb.is_active = true
                    AND (%s = '' OR vc.base_category_id = %s)
                GROUP BY v.id, v.rating
                ORDER BY v.rating DESC
                LIMIT %s
                OFFSET %s"""

        c_sql = """SELECT COUNT(DISTINCT v.id) as bookmark_count
                FROM venue_bookmark vb
                INNER JOIN venue v on vb.venue_id = v.id
                INNER JOIN venue_category vc on vc.id = v.cat_id
                WHERE
                    vb.city_id = %s
                    AND vb.user_id = %s
                    AND vb.is_active = true
                    AND (%s = '' OR vc.base_category_id = %s)"""

        base_cat_id = fs_consts.CAT_DICT.get(section, '')
        limit = page_size
        offset = (page_num-1) * page_size

        # Execute Query to get venues
        res = exec_sql(sql, [city_id, user_id, base_cat_id, base_cat_id, limit, offset])
        venue_ids = [v['id'] for v in res]

        # Execute Query to total number of results
        res_count = exec_sql(c_sql, [city_id, user_id, base_cat_id, base_cat_id])
        total_count = res_count[0]['bookmark_count']

        return venue_ids, total_count

    def save_bookmark(self, user_id, venue_id, city_id, is_active):
        """Save bookmark"""
        # make sure the venue exists
        try:
            models.Venue.objects.get(pk=venue_id)
        except models.Venue.DoesNotExist:
            return None

        try:
            b = models.VenueBookmark.objects.get(user_id=user_id, venue_id=venue_id)
            b.is_active = is_active
        except models.VenueBookmark.DoesNotExist:
            b = models.VenueBookmark(user_id=user_id,
                                     venue_id=venue_id,
                                     city_id=city_id,
                                     is_active=is_active)
        b.save()
        return b