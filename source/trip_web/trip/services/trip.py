"""Trip Services"""

from cor.db import get_or_none
from .. import models

import logging
log = logging.getLogger(__name__)


class TripService(object):
    """Trip Service"""

    # ===============================================================================
    # Public Methods
    # ===============================================================================

    def add_city(self, trip_id,
                 city_id, city_name, city_admin_name, country_name,
                 user_id, check_security=True):
        """Add a city to a trip"""
        if check_security and not self._is_member(trip_id, user_id):
            return 401, None

        city = self._add_city(trip_id=trip_id,
                              city_id=city_id,
                              name=city_name,
                              admin_name=city_admin_name,
                              country_name=country_name,
                              user_id=user_id)
        return 200, city

    def add_members(self, trip_id, member_ids, user_id, check_security=True):
        """Add a member to a trip"""
        if check_security and not self._is_member(trip_id, user_id):
            return 401, None

        members = self._add_members(trip_id, member_ids, user_id)
        return 200, members

    def create_trip(self, name, user_id,
                    city_id=None, city_name=None,
                    city_admin_name=None, country_name=None,
                    start_dt=None, end_dt=None):
        """Create a trip"""
        t = models.Trip(name=name,
                        start_dt=start_dt,
                        end_dt=end_dt,
                        create_user_id=user_id)
        t.save()

        self._add_member(t.id, user_id, user_id)

        if city_id is not None:
            self._add_city(trip_id=t.id,
                           city_id=city_id,
                           name=city_name,
                           admin_name=city_admin_name,
                           country_name=country_name,
                           user_id=user_id)
        return t

    def get_cities(self, trip_id, user_id, active=True, check_security=True):
        """Get Cities for a trip"""
        if check_security and not self._is_member(trip_id, user_id):
            return 401, None

        q = models.TripCity.objects
        q = q.filter(trip_id=trip_id, is_active=active)
        return 200, q[:]

    def get_members(self, trip_id, user_id, active=True, check_security=True):
        """Get members for a trip"""
        if check_security and not self._is_member(trip_id, user_id):
            return 401, None

        q = models.TripUser.objects
        q = q.filter(trip_members__trip_id=trip_id, trip_members__is_active=active)
        return 200, q[:]

    def get_trip(self, trip_id, user_id):
        """Get a trip"""
        t = get_or_none(models.Trip.objects, pk=trip_id)
        if t is None:
            log.warning("Trip %s was not found" % trip_id)
            status = 404
        elif self._is_member(trip_id, user_id):
            status = 200
        else:
            log.warning("user %s cannot access trip %s" % (user_id, trip_id))
            status = 401
        return status, t if status == 200 else None

    # ===============================================================================
    # Private Methods
    # ===============================================================================

    def _add_city(self, trip_id, city_id, name, admin_name, country_name, user_id):
        """Add a City"""
        q = models.TripCity.objects
        city, created = q.get_or_create(trip_id=trip_id,
                                        city_id=city_id,
                                        defaults={
                                            'name': name,
                                            'admin_name': admin_name,
                                            'country_name': country_name,
                                            'create_user_id': user_id
                                        })
        if not created:
            city.is_active = True
            city.update_user_id = user_id
            city.save()
        return city

    def _add_member(self, trip_id, member_id, user_id):
        """Add a Member"""
        q = models.TripMember.objects
        member, created = q.get_or_create(trip_id=trip_id,
                                          member_id=member_id,
                                          defaults={'create_user_id': user_id})
        if not created:
            member.is_active = True
            member.update_user_id = user_id
            member.save()
        return member

    def _add_members(self, trip_id, member_ids, user_id):
        """Add Members"""
        members = []
        for member_id in member_ids:
            member = self._add_member(trip_id, member_id, user_id)
            members.append(member)
        return members

    def _is_member(self, trip_id, user_id):
        """Check if user is a member of the trip"""
        q = models.TripMember.objects
        q = q.filter(trip_id=trip_id)
        q = q.filter(member_id=user_id)
        q = q.filter(is_active=True)
        c = q.count()
        return c == 1