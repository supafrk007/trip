"""Venues Services"""

from cor.location.foursquare import FoursquareHelper
from datetime import datetime, timedelta
from .. import models

import logging
log = logging.getLogger(__name__)

DEFAULT_GT_DAYS = 1


class VenuesService(object):
    """Venues Service"""

    # ===============================================================================
    # FS Methods
    # ===============================================================================

    def _fs_explore(self, near, section, limit, offset,
                    access_token=None, client_id=None, secret=None):
        """Explore Venues from Foursquare"""
        fs_q = {'near': near,
                'venuePhotos': 1,
                'limit': limit,
                'offset': offset}

        if section is not None:
            fs_q['section'] = section

        helper = FoursquareHelper(client_id=client_id, secret=secret, access_token=access_token)
        r = helper.explore_venues(fs_q)
        return r

    def _save_fs_explore_venues(self, exp_resp, city_id, q, page_num):
        """Save FS venues to db"""
        import json

        total_venues = exp_resp.total_results
        s = self._save_search(city_id=city_id, q=q, page_num=page_num, total_venues=total_venues)
        self._del_search_maps(s.id)
        #maps = []
        sort_order = 1
        for v in exp_resp.venues:
            id = v.venue['id']
            name = v.venue['name']
            lat = v.venue['location']['lat']
            lng = v.venue['location']['lng']
            rating = v.venue['rating'] if 'rating' in v.venue else None
            contact = v.venue['contact']
            location = json.dumps(v.venue['location'])
            photo_url_prefix = None
            photo_url_suffix = None
            if 'photos' in v.venue:
                #print(v.venue['photos']['groups'])
                if len(v.venue['photos']['groups'][0]['items']) > 0:
                    photo_url_prefix = v.venue['photos']['groups'][0]['items'][0]['prefix']
                    photo_url_suffix = v.venue['photos']['groups'][0]['items'][0]['suffix']
            url = v.venue['url'] if 'url' in v.venue else None
            if 'tips' in v.venue:
                tip_text = v.venue['tips'][0]['text'] if len(v.venue['tips']) > 0 else None
            else:
                tip_text = None
            category_id = v.venue['categories'][0]['id'] if len(v.venue['categories']) > 0 else None

            self._save_venue(id=id, name=name,
                            category_id=category_id,
                            lat=lat, lng=lng,
                            rating=rating,
                            contact=contact, location=location,
                            photo_url_prefix=photo_url_prefix,
                            photo_url_suffix=photo_url_suffix,
                            url=url, tip_text=tip_text)
            self._save_search_map(search_id=s.id, venue_id=id, sort_order=sort_order)
            sort_order += 1

    # ===============================================================================
    # Public Methods
    # ===============================================================================

    def explore(self, city_id, near, section, page_num, page_size,
                access_token=None, client_id=None, secret=None,
                #load_bookmarks=False,
                user_id=None):
        """Explore Venues"""
        log.debug('--> VenuesService.explore')

        offset = (page_num-1)*15

        # try to get a VenueSearch
        q = '/x/:s'.replace(':s', section if section is not None else 'all')
        s = self._get_search(city_id, q, page_num, DEFAULT_GT_DAYS)

        # load from FS if they do not exist
        if s is None:
            log.debug('Missed Cache. Hitting FS')
            r = self._fs_explore(near=near, section=section,
                                limit=page_size, offset=offset,
                                access_token=access_token,
                                client_id=client_id, secret=secret)
            self._save_fs_explore_venues(r, city_id, q, page_num)
            s = self._get_search(city_id, q, page_num)

        # load maps
        search_id = s.id
        maps = self._get_search_maps(search_id=search_id)

        venues = [m.venue for m in maps]
        total_venues = s.total_venues

        return total_venues, venues

    def get_category(self, category_id):
        """Get a Venue Category"""
        log.debug('--> VenuesService.get_category(%s)' % category_id)
        return models.VenueCategory.objects.get(pk=category_id)

    def get_venues(self, venue_ids):
        """Get a list of venues"""
        return models.Venue.objects.filter(id__in=venue_ids).order_by('-rating')[:]

    def load_categories(self, access_token=None, client_id=None, secret=None):
        """Load Venue Categories into the DB"""
        helper = FoursquareHelper(client_id=client_id, secret=secret, access_token=access_token)
        r = helper.get_venue_categories()
        cats = r.categories
        for c in cats:
            self._save_category(c)


    #===============================================================================
    # Private Methods
    #===============================================================================

    def _del_search_maps(self, search_id):
        """Delete Venue Search Maps"""
        models.VenueSearchMap.objects.filter(search_id=search_id).delete()

    def _get_search(self, city_id, q, page_num, gt_days=None):
        """Get a Venu Search"""
        qs = models.VenueSearch.objects.filter(city_id=city_id)
        qs = qs.filter(page_num=page_num)
        if gt_days is not None:
            time_threshold = datetime.now() - timedelta(days=gt_days)
            qs = qs.filter(update_dt__gte=time_threshold)

        try:
            s = qs.get(q=q)
        except models.VenueSearch.DoesNotExist:
            s = None

        return s

    def _get_search_maps(self, search_id):
        """Get Venues that have been cached in the database"""
        maps_q = models.VenueSearchMap.objects.filter(search_id=search_id).order_by('sort_order')
        return maps_q[:]

    def _save_venue(self, id, name, category_id, lat, lng,
                    rating, contact, location,
                    photo_url_prefix, photo_url_suffix,
                    url, tip_text):
        """Save a Venue"""
        try:
            v = models.Venue.objects.get(pk=id)
        except models.Venue.DoesNotExist:
            v = models.Venue(id=id)
        cat = self.get_category(category_id)
        v.name = name
        v.cat = cat
        v.lat = lat
        v.lng = lng
        v.rating = rating
        v.contact = contact
        v.location = location
        v.photo_url_prefix = photo_url_prefix
        v.photo_url_suffix = photo_url_suffix
        v.url = url
        v.tip_text = tip_text
        v.save()
        return v

    def _save_category(self, c, base_id=None, parent_id=None):
        """Save a FS Category to the DB"""
        try:
            db_c = models.VenueCategory.objects.get(id=c.id)
        except models.VenueCategory.DoesNotExist:
            db_c = models.VenueCategory(id=c.id)

        db_c.base_category_id = base_id
        db_c.parent_category_id = parent_id
        db_c.name = c.name
        db_c.plural_name = c.plural_name
        db_c.short_name = c.short_name
        db_c.save()

        for child in c.categories:
            self._save_category(child,
                                base_id=c.id if base_id is None else base_id,
                                parent_id=c.id)

    def _save_search(self, city_id, q, page_num, total_venues):
        """Save Venue Search"""
        try:
            s = models.VenueSearch.objects.get(city_id=city_id, q=q, page_num=page_num)
            s.total_venues = total_venues
        except models.VenueSearch.DoesNotExist:
            s = models.VenueSearch(city_id=city_id,
                                   q=q,
                                   page_num=page_num,
                                   total_venues=total_venues)
        s.save()
        return s

    def _save_search_map(self, search_id, venue_id, sort_order):
        """Save a VenueSearchMap"""
        try:
            m = models.VenueSearchMap.objects.get(search_id=search_id,
                                                  venue_id=venue_id, sort_order=sort_order)
        except models.VenueSearchMap.DoesNotExist:
            m = models.VenueSearchMap(search_id=search_id,
                                      venue_id=venue_id,
                                      sort_order=sort_order)
        m.save()
        return m