# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0002_tripuser_fb_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='Venue',
            fields=[
                ('id', models.CharField(primary_key=True, verbose_name='Foursquare Venue ID', max_length=36, serialize=False)),
                ('name', models.CharField(verbose_name='Name', max_length=200)),
                ('lat', models.DecimalField(decimal_places=14, verbose_name='Latitude', max_digits=17)),
                ('lng', models.DecimalField(decimal_places=14, verbose_name='Longitude', max_digits=17)),
                ('rating', models.DecimalField(decimal_places=2, null=True, verbose_name='Rating', blank=True, max_digits=4)),
                ('contact', models.CharField(verbose_name='JSON Contact Info', max_length=500)),
                ('location', models.CharField(verbose_name='JSON Location', max_length=500)),
                ('photo_url_prefix', models.CharField(null=True, blank=True, verbose_name='Photo URL prefix', max_length=75)),
                ('photo_url_suffix', models.CharField(null=True, blank=True, verbose_name='Photo URL suffix', max_length=75)),
                ('url', models.URLField(null=True, blank=True, verbose_name='Site URL')),
                ('tip_text', models.TextField(null=True, verbose_name='Tip Text', blank=True)),
                ('create_dt', models.DateTimeField(auto_now_add=True, verbose_name='Create Date')),
                ('update_dt', models.DateTimeField(verbose_name='Update Date', auto_now=True)),
            ],
            options={
                'db_table': 'venue',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VenueCategory',
            fields=[
                ('id', models.CharField(primary_key=True, verbose_name='Foursquare Category ID', max_length=36, serialize=False)),
                ('name', models.CharField(verbose_name='Name', max_length=50)),
                ('plural_name', models.CharField(verbose_name='Plural Name', max_length=52)),
                ('short_name', models.CharField(verbose_name='Short Name', max_length=52)),
                ('create_dt', models.DateTimeField(auto_now_add=True, verbose_name='Create Date')),
                ('update_dt', models.DateTimeField(verbose_name='Update Date', auto_now=True)),
                ('base_category', models.ForeignKey(related_name='+', blank=True, to='trip.VenueCategory', null=True)),
                ('parent_category', models.ForeignKey(related_name='+', blank=True, to='trip.VenueCategory', null=True)),
            ],
            options={
                'db_table': 'venue_category',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VenueSearch',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('city_id', models.IntegerField(verbose_name='City Id')),
                ('q', models.CharField(verbose_name='Query Representation', max_length=100)),
                ('page_num', models.SmallIntegerField(verbose_name='Page Number')),
                ('total_venues', models.SmallIntegerField(verbose_name='Total Venues')),
                ('create_dt', models.DateTimeField(auto_now_add=True, verbose_name='Create Date')),
                ('update_dt', models.DateTimeField(verbose_name='Update Date', auto_now=True)),
            ],
            options={
                'db_table': 'venue_search',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VenueSearchMap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('sort_order', models.SmallIntegerField(verbose_name='Sort Order')),
                ('create_dt', models.DateTimeField(auto_now_add=True, verbose_name='Create Date')),
                ('update_dt', models.DateTimeField(verbose_name='Update Date', auto_now=True)),
                ('search', models.ForeignKey(to='trip.VenueSearch')),
                ('venue', models.ForeignKey(to='trip.Venue')),
            ],
            options={
                'db_table': 'venue_search_map',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='venuesearchmap',
            unique_together=set([('search', 'venue')]),
        ),
        migrations.AlterUniqueTogether(
            name='venuesearch',
            unique_together=set([('city_id', 'q', 'page_num')]),
        ),
        migrations.AddField(
            model_name='venue',
            name='cat',
            field=models.ForeignKey(to='trip.VenueCategory'),
            preserve_default=True,
        ),
    ]
