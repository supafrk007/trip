# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0005_trip_tripappointment_tripcity_tripmember'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trip',
            name='user',
        ),
        migrations.AlterField(
            model_name='tripmember',
            name='member',
            field=models.ForeignKey(related_query_name='trip_members', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tripmember',
            name='trip',
            field=models.ForeignKey(to='trip.Trip'),
            preserve_default=True,
        ),
    ]
