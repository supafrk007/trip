# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0003_auto_20150303_0630'),
    ]

    operations = [
        migrations.CreateModel(
            name='VenueBookmark',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('city_id', models.IntegerField(verbose_name='City Id', db_index=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='Is Bookmark Active')),
                ('create_dt', models.DateTimeField(auto_now_add=True, verbose_name='Create Date')),
                ('update_dt', models.DateTimeField(auto_now=True, verbose_name='Update Date')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('venue', models.ForeignKey(to='trip.Venue')),
            ],
            options={
                'db_table': 'venue_bookmark',
            },
            bases=(models.Model,),
        ),
    ]
