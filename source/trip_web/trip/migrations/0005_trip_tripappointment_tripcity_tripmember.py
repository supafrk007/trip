# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0004_venuebookmark'),
    ]

    operations = [
        migrations.CreateModel(
            name='Trip',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(verbose_name='Trip Name', max_length=250)),
                ('start_dt', models.DateTimeField(null=True, verbose_name='Start Date', blank=True)),
                ('end_dt', models.DateTimeField(null=True, verbose_name='End Date', blank=True)),
                ('create_dt', models.DateTimeField(verbose_name='Create Date', auto_now_add=True)),
                ('update_dt', models.DateTimeField(verbose_name='Update Date', auto_now=True)),
                ('create_user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='+')),
                ('update_user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, blank=True, related_name='+')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'trip',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TripAppointment',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('start_dt', models.DateTimeField(verbose_name='Start Date')),
                ('end_dt', models.DateTimeField(null=True, verbose_name='End Date', blank=True)),
                ('is_active', models.BooleanField(verbose_name='Is Appointment Active', default=True)),
                ('is_all_day', models.BooleanField(verbose_name='Is All Day Appt', default=True)),
                ('create_dt', models.DateTimeField(verbose_name='Create Date', auto_now_add=True)),
                ('update_dt', models.DateTimeField(verbose_name='Update Date', auto_now=True)),
                ('create_user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='+')),
                ('trip', models.ForeignKey(to='trip.Trip', related_name='trip_appointments')),
                ('update_user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, blank=True, related_name='+')),
                ('venue', models.ForeignKey(to='trip.Venue')),
            ],
            options={
                'db_table': 'trip_appointment',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TripCity',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('city_id', models.IntegerField(verbose_name='City Id', db_index=True)),
                ('name', models.CharField(verbose_name='City Name', max_length=250)),
                ('admin_name', models.CharField(verbose_name='Admin Name', max_length=250)),
                ('country_name', models.CharField(verbose_name='Country Name', max_length=250)),
                ('is_active', models.BooleanField(verbose_name='Is Bookmark Active', default=True)),
                ('create_dt', models.DateTimeField(verbose_name='Create Date', auto_now_add=True)),
                ('update_dt', models.DateTimeField(verbose_name='Update Date', auto_now=True)),
                ('create_user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='+')),
                ('trip', models.ForeignKey(to='trip.Trip', related_name='trip_cities')),
                ('update_user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, blank=True, related_name='+')),
            ],
            options={
                'db_table': 'trip_city',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TripMember',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(verbose_name='Is Bookmark Active', default=True)),
                ('create_dt', models.DateTimeField(verbose_name='Create Date', auto_now_add=True)),
                ('update_dt', models.DateTimeField(verbose_name='Update Date', auto_now=True)),
                ('create_user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='+')),
                ('member', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('trip', models.ForeignKey(to='trip.Trip', related_name='trip_members')),
                ('update_user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, blank=True, related_name='+')),
            ],
            options={
                'db_table': 'trip_member',
            },
            bases=(models.Model,),
        ),
    ]
