# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('facebook', '__first__'),
        ('trip', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tripuser',
            name='fb_user',
            field=models.OneToOneField(blank=True, to='facebook.FBUser', null=True),
            preserve_default=True,
        ),
    ]
