"""
Django settings for trip_web project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'w%$c+*88_h3d^engf%e4qt(y7rqk&899owc(wuq9%y)kh(c**g'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework.authtoken',
    
    'cor.social.facebook',
    'trip'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'trip_web.urls'
WSGI_APPLICATION = 'trip_web.wsgi.application'

AUTH_USER_MODEL = 'trip.TripUser'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'seed_db',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'seed_dbo',
        'PASSWORD': 'cokecan3',
        'HOST': 'localhost',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '5432',                      # Set to empty string for default.
        'OPTIONS': {
            'autocommit': True,
        },
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, 'static'),
)

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, "templates"),
)

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',
    ), 
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.TokenAuthentication',
        #'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
}


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
   'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'simple',
            'stream': sys.stderr
        },
#         # Log to a text file that can be rotated by logrotate
#        'logfile': {
#            'class': 'logging.handlers.WatchedFileHandler',
#            'filename': '/var/log/django/myapp.log'
#        },
    },
    'loggers': {
        'null': {
            'handlers': ['console'],
            'level': 'DEBUG', # Or maybe INFO or DEBUG
            'propagate': True
        },
        # Again, default Django configuration to email unhandled exceptions
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        # Might as well log any errors anywhere else in Django
        'django': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False,
        },
        # Your own app - this assumes all your logger names start with "myapp."
        'trip': {
            'handlers': ['console'],
            'level': 'DEBUG', # Or maybe INFO or DEBUG
            'propagate': True
        },
        'cor': {
            'handlers': ['console'],
            'level': 'DEBUG', # Or maybe INFO or DEBUG
            'propagate': True
        },
    }
}




# EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_PORT = '587'
# EMAIL_HOST_USER = 'bkhan1@gmail.com'
# EMAIL_HOST_PASSWORD = 'kbpcxfemwnfkmyhk'
# EMAIL_USE_TLS = True
# 
# FROM_EMAIL = 'bkhan1@gmail.com'
# SITE_ROOT = 'http://127.0.0.1:8000'

#STRIPE_PUB_KEY = 'pk_0AOP834VXA22nrY3IjgMKeCb8wlTq'
#STRIPE_SECRET_KEY = 'sk_0AOPUHloqV4I1fHIsXSgWQDjVWV3r'
#
FB_APP_ID = '198310316874063'
FB_APP_SECRET = '5493c6d795924fe785bbf71e947a2094'
FB_LOGIN_URL = 'http://localhost:8000/fb_login'
FB_REGISTER_URL = 'http://localhost:8000/fb_register'
FB_SCOPE = 'email,offline_access,read_stream'

FSQ_CLIENT_ID = 'H3ZKK051Q4B3AMRW0KGLLTTAZ2H4UONPB3ZPT2WUJA3IV53R'
FSQ_SECRET = 'TPMRNFNZOM1MUFF20DRNZDD50UXACRKEZ4XFK1EKFTOIV5GV'
FSQ_LOGIN_URL = 'http://localhost:8000/fsq_redirect'
FSQ_ACCESS_URL = 'https://foursquare.com/oauth2/access_token'
FSQ_AUTHORIZE = 'https://foursquare.com/oauth2/authorize'

GEONAMES_USERNAME = 'dumdummy9'