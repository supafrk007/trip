from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from trip.views import bookmarks as bookmark_views
from trip.views import city as city_views
from trip.views import trip as trip_views
from trip.views import users as user_views
from trip.views import venues as venues_views

#from authentication.views import SiteUserFBTokenRegister
#from django.contrib import admin


urlpatterns = patterns('',
    # Examples:
    # url(r'^blog/', include('blog.urls')),
    url(r'^api/cities$', city_views.CitiesList.as_view(), name='cities'),
    url(r'^api/city/(?P<city_id>.+)/bookmarks$', city_views.CityBookmarkList.as_view(), name='city_bookmarks'),
    url(r'^api/city/(?P<city_q>.+)$', city_views.CityDetail.as_view(), name='city'),
    url(r'^api/trip/(?P<trip_id>.+)$', trip_views.TripDetail.as_view(), name='trip'),
    url(r'^api/trip', trip_views.TripCreator.as_view(), name='trip_create'),
    url(r'^api/users', user_views.UserDetail.as_view(), name='users'),
    url(r'^api/venues', venues_views.VenuesList.as_view(), name='venues'),
    url(r'^api/venue/(?P<venue_id>.+)/bookmark$', bookmark_views.BookmarkDetail.as_view(), name='bookmark'),
    url(r'^.*$', TemplateView.as_view(template_name='index.html')),

    url(r'^admin/', include(admin.site.urls)),
)
